<?php
/*
	Programmer	: Tiar Aristian
	Release		: Januari 2016
	Module		: Helper Controller -> Controller untuk modul halaman helper
*/
class HelperController extends Zend_Controller_Action
{
	function init()
	{
		$this->initView();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		// load class
		Zend_Loader::loadClass('Zend_Session');
		Zend_Loader::loadClass('Customer');
		Zend_Loader::loadClass('Rumah');
		Zend_Loader::loadClass('Cluster');
		Zend_Loader::loadClass('TipeKas');
		Zend_Loader::loadClass('Supplier');
		Zend_Loader::loadClass('Po');
		Zend_Loader::loadClass('Unit');
		Zend_Loader::loadClass('Satuan');
		Zend_Loader::loadClass('Item');
		Zend_Loader::loadClass('KategoriUnit');
		Zend_Loader::loadClass('Kontak');
		Zend_Loader::loadClass('Branch');
		Zend_Loader::loadClass('ItemClass');
		Zend_Loader::loadClass('Kas');
		Zend_Loader::loadClass('TypeKontak');
		Zend_Loader::loadClass('Oincome');
		
		// layout
		$this->_helper->layout()->setLayout('source');
	}
	
	public function convertCaller($caller) {
		$i=1;
		$readyCaller='';
		foreach ($caller as $dataCaller) {
			if($i==1){
				$readyCaller='{'.$dataCaller;
			}elseif ($i==count($caller)) {
				$readyCaller=$readyCaller.','.$dataCaller.'}';
			}else{
				$readyCaller=$readyCaller.','.$dataCaller;
			}
			$i++;
		}
		return $readyCaller;
	}

	function indexAction(){
		// title
		$this->view->title="Halaman Data";
	}
	
	function customerAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$customer = new Customer();
		$this->view->listCustomer=$customer->fetchAll();
		// title
		$this->view->title="Daftar Customer";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function rumahAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$rumah = new Rumah();
		$this->view->listRumah=$rumah->fetchAll();
		// title
		$this->view->title="Daftar Rumah";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function clusterAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$cluster = new Cluster();
		$this->view->listCluster=$cluster->fetchAll();
		// title
		$this->view->title="Daftar Cluster";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function tipekasAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$tipekas = new TipeKas();
		$this->view->listTipeKas=$tipekas->fetchAll();
		// title
		$this->view->title="Daftar Tipe Kas";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function poAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$po = new Po();
		$this->view->listPo=$po->fetchAll();
		// title
		$this->view->title="Daftar Po";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function supplierAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$supplier = new Supplier();
		$this->view->listSupplier=$supplier->fetchAll();
		// title
		$this->view->title="Daftar Supplier";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function unitAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$unit = new Unit();
		$this->view->listUnit=$unit->fetchAll();
		// title
		$this->view->title="Daftar Unit";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function satuanAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$satuan = new Satuan();
		$this->view->listSatuan=$satuan->fetchAll();
		// title
		$this->view->title="Daftar Satuan";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function itemAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$item = new Item();
		$this->view->listItem=$item->fetchAll();
		// title
		$this->view->title="Daftar Item";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function kategoriunitAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$kategoriunit = new KategoriUnit();
		$this->view->listKategoriUnit=$kategoriunit->fetchAll();
		// title
		$this->view->title="Daftar KategoriUnit";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function kontakAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$kontak = new Kontak();
		$this->view->listKontak=$kontak->fetchAll();
		// title
		$this->view->title="Daftar Debitur";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function branchAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$branch = new Branch();
		$this->view->listBranch=$branch->fetchAll();
		// title
		$this->view->title="Daftar Branch";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function itemclassAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$itemclass = new ItemClass();
		$this->view->listItemClass=$itemclass->fetchAll();
		// title
		$this->view->title="Daftar Item Class";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function kasAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$kas = new Kas();
		$this->view->listKas=$kas->fetchAll();
		// title
		$this->view->title="Daftar Kas";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function tipekontakAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$typekontak = new TypeKontak();
		$this->view->listTypeKontak=$typekontak->fetchAll();
		// title
		$this->view->title="Daftar Tipe Kontak";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}
	function oincomeAction(){
		// get param
		$request = $this->getRequest()->getPost();
		//gets value from ajax request
		$caller=$request['caller'];
		$oincome = new Oincome();
		$this->view->listOincome=$oincome->fetchAll();
		// title
		$this->view->title="Daftar Oincome";
		// convert variabel caller
		$readyCaller=$this->convertCaller($caller);
		// throw variabel caller to view
		$this->view->readyCaller=$readyCaller;
	}

}
?>