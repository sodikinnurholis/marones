<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class PenerimaanLainController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('PenerimaanLain');
        Zend_Loader::loadClass('Kas');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_trs="active";
        $this->view->active_ri="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "PenerimaanLain";
        // navigation
        $this->_helper->navbar(0,0,'penerimaanlain/new',0,0);
        Zend_Session::namespaceUnset('par_penerimaanlain');
        $kas=new Kas();
        $this->view->listKas=$kas->fetchAll();
    }

    function ashowAction(){
        // makes disable layout
        $this->_helper->getHelper('layout')->disableLayout();
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $id_kas=$request['id_kas'];
        $tgl1=$request['tgl1'];
        $tgl2=$request['tgl2'];
        // set session
        $param = new Zend_Session_Namespace('par_penerimaanlain');
        $param->id_kas=$id_kas;
        $param->tgl1=$tgl1;
        $param->tgl2=$tgl2;
    }
    
    function listAction(){
        // Title Browser
        $this->view->title = "Daftar PenerimaanLain";
        // Navbar
        $this->_helper->navbar('penerimaanlain',0,'penerimaanlain/new',0,0);
        // collapse
        $this->view->sidecolaps="sidebar-collapse";
        // get param
        $param = new Zend_Session_Namespace('par_penerimaanlain');
        $id_kas=$param->id_kas;
        $tgl1=$param->tgl1;
        $tgl2=$param->tgl2;
        // get data
        $penerimaanlain=new PenerimaanLain();
        $getPenerimaanLain=$penerimaanlain->queryPenerimaanLain($id_kas,$tgl1, $tgl2);
        $this->view->listPenerimaanLain=$getPenerimaanLain;
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data PenerimaanLain";
        // navigation
        $this->_helper->navbar(0,'penerimaanlain',0,0,0);
        // ri
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_penerimaan_lain = $this->_helper->string->esc_quote(trim($request['id_penerimaan_lain']));
        $no_penerimaan_lain = $this->_helper->string->esc_quote(trim($request['no_penerimaan_lain']));
        $tgl_penerimaan = $this->_helper->string->esc_quote(trim($request['tgl_penerimaan']));
        $id_kas = $this->_helper->string->esc_quote(trim($request['id_kas']));
        $id_oincome = $this->_helper->string->esc_quote(trim($request['id_oincome']));
        $keterangan = $this->_helper->string->esc_quote(trim($request['keterangan']));
        $nominal = $request['nominal'];
        $no_bukti = $this->_helper->string->esc_quote(trim($request['no_bukti']));
        $status = $this->_helper->string->esc_quote(trim($request['status']));
       
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        
        if($vd->validasiLength($id_kas,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Kas tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_kas,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Kas tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($nominal_sa,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nominal Saldo tidak boleh kosong maksimal 50 angka</strong><br>";
        }
        if($vd->validasiLength($tgl_saldo_awal,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Tanggal Saldo tidak boleh kosong</strong><br>";
        }
       
         if($err==0){
            $penerimaanlain = new PenerimaanLain();
           $setPenerimaanLain = $penerimaanlain->setPenerimaanLain($id_kas,$nominal_sa,$tgl_saldo_awal);
            echo $setPenerimaanLain;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $penerimaanlain = new PenerimaanLain();
        $delPenerimaanLain=$penerimaanlain->delPenerimaanLain($id);
        echo $delPenerimaanLain;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data PenerimaanLain";
        // Navbar
        $this->_helper->navbar('penerimaanlain',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $penerimaanlain=new PenerimaanLain();
        $getPenerimaanLain=$penerimaanlain->getPenerimaanLainById($id);
        if(!$getPenerimaanLain){
            $this->view->eksis="f";
        }else{
            foreach ($getPenerimaanLain as $data){
                $this->view->id_kas=$data['id_kas'];
                $this->view->nm_kas=$data['nm_kas'];
                $this->view->nm_branch=$data['nm_branch'];
                $this->view->nominal_sa=$data['nominal_sa'];
                $this->view->tgl=$data['tgl_saldo_awal_fmt'];
            }
            $this->view->title = "Edit Data Saldo Awal Kas ";
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_kas = $request['id_kas'];
        $tgl_saldo_awal = $this->_helper->string->esc_quote(trim($request['tgl_saldo_awal']));
        $nominal_sa = $request['nominal_sa'];
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        
        
        if($vd->validasiLength($nominal_sa,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nominal Saldo tidak boleh kosong maksimal 50 angka</strong><br>";
        }
        if($vd->validasiLength($tgl_saldo_awal,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Tanggal Saldo tidak boleh kosong</strong><br>";
        }
         if($err==0){
            $penerimaanlain = new PenerimaanLain();
           $setPenerimaanLain = $penerimaanlain->updPenerimaanLain($nominal_sa,$tgl_saldo_awal,$id_kas);
            echo $setPenerimaanLain;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}