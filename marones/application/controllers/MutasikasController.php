<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class MutasikasController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('MutasiKas');
        Zend_Loader::loadClass('Kas');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mts="active";
        $this->view->active_mutasikas="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Mutasi Kas";
        // navigation
        $this->_helper->navbar(0,0,'mutasikas/new',0,0);
        Zend_Session::namespaceUnset('par_mutasikas');
        $kas=new Kas();
        $this->view->listKas=$kas->fetchAll();
        $kas_to=new Kas();
        $this->view->listKas_to=$kas_to->fetchAll();
    }

    function ashowAction(){
        // makes disable layout
        $this->_helper->getHelper('layout')->disableLayout();
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $id_kas_from=$request['id_kas_from'];
        $id_kas_to=$request['id_kas_to'];
        $tgl1=$request['tgl1'];
        $tgl2=$request['tgl2'];
        // set session
        $param = new Zend_Session_Namespace('par_mutasikas');
        $param->id_kas_from=$id_kas_from;
        $param->id_kas_to=$id_kas_to;
        $param->tgl1=$tgl1;
        $param->tgl2=$tgl2;
    }
    
    function listAction(){
        // Title Browser
        $this->view->title = "Daftar Mutasi Kas";
        // Navbar
        $this->_helper->navbar('mutasikas',0,'mutasikas/new',0,0);
        // collapse
        $this->view->sidecolaps="sidebar-collapse";
        // get param
        $param = new Zend_Session_Namespace('par_mutasikas');
        $id_kas_from=$param->id_kas_from;
        $id_kas_to=$param->id_kas_to;
        $tgl1=$param->tgl1;
        $tgl2=$param->tgl2;
        // get data
        $mutasikas=new MutasiKas();
        $getMutasiKas=$mutasikas->queryMutasiKas($id_kas_from,$id_kas_to, $tgl1, $tgl2);
        $this->view->listMutasiKas=$getMutasiKas;
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Mutasi Kas";
        // navigation
        $this->_helper->navbar(0,'mutasikas',0,0,0);
        // ri
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $tgl_mutasi = $this->_helper->string->esc_quote(trim($request['tgl_mutasi']));
        $id_kas_from = $this->_helper->string->esc_quote(trim($request['id_kas_from']));
        $id_kas_to = $this->_helper->string->esc_quote(trim($request['id_kas_to']));
        $nominal = $request['nominal'];
        $keterangan = $this->_helper->string->esc_quote(trim($request['keterangan']));
        $no_bukti = $this->_helper->string->esc_quote(trim($request['no_bukti']));
        $status = '0';
       
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        
        if($vd->validasiLength($tgl_mutasi,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Tanggal Saldo tidak boleh kosong</strong><br>";
        }
        if($vd->validasiLength($id_kas_from,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Kas Masuk tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_kas_to,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Kas Keluar tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($nominal,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nominal tidak boleh kosong maksimal 50 angka</strong><br>";
        }
        if($vd->validasiLength($keterangan,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Keterangan tidak boleh kosong maksimal 50 angka</strong><br>";
        }
        if($vd->validasiLength($no_bukti,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- No Bukti tidak boleh kosong maksimal 50 angka</strong><br>";
        }
        
       
         if($err==0){
            $mutasikas = new MutasiKas();
           $setMutasiKas = $mutasikas->setMutasiKas($tgl_mutasi,$id_kas_from,$id_kas_from,$nominal,$keterangan,$no_bukti,$status);
            echo $setMutasiKas;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $mutasikas = new MutasiKas();
        $delMutasiKas=$mutasikas->delMutasiKas($id);
        echo $delMutasiKas;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Mutasi Kas";
        // Navbar
        $this->_helper->navbar('mutasikas',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $mutasikas=new MutasiKas();
        $getMutasiKas=$mutasikas->getMutasiKasById($id);
        if(!$getMutasiKas){
            $this->view->eksis="f";
        }else{
            foreach ($getMutasiKas as $data){
                $this->view->id_mutasi_kas=$data['id_mutasi_kas'];
                $this->view->nm_kas_from=$data['nm_kas_from'];
                $this->view->id_kas_from=$data['id_kas_from'];
                $this->view->id_kas_to=$data['id_kas_to'];
                $this->view->nm_kas_to=$data['nm_kas_to'];
                $this->view->nominal=$data['nominal'];
                $this->view->keterangan=$data['keterangan'];
                $this->view->no_bukti=$data['no_bukti'];
            }
            $this->view->title = "Edit Data Mutasi Kas ";
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_mutasi_kas = $request['id_mutasi_kas'];
        $tgl_mutasi = $this->_helper->string->esc_quote(trim($request['tgl_mutasi']));
        $id_kas_from = $this->_helper->string->esc_quote(trim($request['id_kas_from']));
        $id_kas_to = $this->_helper->string->esc_quote(trim($request['id_kas_to']));
        $nominal = $request['nominal'];
        $keterangan = $this->_helper->string->esc_quote(trim($request['keterangan']));
        $no_bukti = $this->_helper->string->esc_quote(trim($request['no_bukti']));
        $status = '0';
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        
        
        if($vd->validasiLength($tgl_mutasi,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Tanggal Saldo tidak boleh kosong</strong><br>";
        }
        if($vd->validasiLength($id_kas_from,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Kas Masuk tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_kas_to,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Kas Keluar tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($nominal,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nominal tidak boleh kosong maksimal 50 angka</strong><br>";
        }
        if($vd->validasiLength($keterangan,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Keterangan tidak boleh kosong maksimal 50 angka</strong><br>";
        }
        if($vd->validasiLength($no_bukti,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- No Bukti tidak boleh kosong maksimal 50 angka</strong><br>";
        }
         if($err==0){
            $mutasikas = new MutasiKas();
           $setMutasiKas = $mutasikas->updMutasiKas($tgl_mutasi,$id_kas_from,$id_kas_to,$nominal,$keterangan,$no_bukti,$status,$id_mutasi_kas);
            echo $setMutasiKas;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}