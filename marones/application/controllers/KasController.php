<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class KasController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Kas');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst="active";
        $this->view->active_kas="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Kas";
        // navigation
        $this->_helper->navbar(0,0,'kas/new',0,0);
        $kas= new Kas();
        $this->view->listKas=$kas->fetchAll();
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Kas";
        // navigation
        $this->_helper->navbar(0,'kas',0,0,0);
        // kas
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $nm_kas = $this->_helper->string->esc_quote(trim($request['nm']));
        $id_tipe_kas = $this->_helper->string->esc_quote(trim($request['id_tipe_kas']));
        $id_branch = $this->_helper->string->esc_quote(trim($request['id_branch']));
        $no_rek = $this->_helper->string->esc_quote(trim($request['no_rek']));
        $pemilik_rek = $this->_helper->string->esc_quote(trim($request['pemilik_rek']));
        $aktif= "t";
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_kas,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama kas tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_tipe_kas,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- ID Tipe Kas tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_branch,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- ID Branch tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($no_rek,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- No Rekening tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($pemilik_rek,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Pemilik Rekening tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $kas = new kas();
           $setkas = $kas->setkas($nm_kas,$id_tipe_kas,$id_branch,$no_rek,$pemilik_rek);
            echo $setkas;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $kas = new Kas();
        $delkas=$kas->delKas($id);
        echo $delkas;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Kas";
        // Navbar
        $this->_helper->navbar('kas',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $kas=new Kas();
        $getkas=$kas->getKasById($id);
        if(!$getkas){
            $this->view->eksis="f";
        }else{
            foreach ($getkas as $data){
                $this->view->id=$data['id_kas'];
                $this->view->nm=$data['nm_kas'];
                $this->view->id_tipe_kas=$data['id_tipe_kas'];
                $this->view->id_branch=$data['id_branch'];
                $this->view->id_kontak=$data['id_kontak'];
                $this->view->nm_tipe_kas=$data['nm_tipe_kas'];
                $this->view->no_rek=$data['no_rek'];
                $this->view->pemilik_rek=$data['pemilik_rek'];
            }
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_kas= $request['id'];
        $nm_kas = $this->_helper->string->esc_quote(trim($request['nm']));
        $id_tipe_kas = $this->_helper->string->esc_quote(trim($request['id_tipe_kas']));
        $id_branch = $this->_helper->string->esc_quote(trim($request['id_branch']));
        $no_rek = $this->_helper->string->esc_quote(trim($request['no_rek']));
        $pemilik_rek = $this->_helper->string->esc_quote(trim($request['pemilik_rek']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_kas,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama kas tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_tipe_kas,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- ID Tipe Kas tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
         if($vd->validasiLength($id_branch,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- ID Branch tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($no_rek,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- No Rekening tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($pemilik_rek,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Pemilik Rekening tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $kas = new kas();
            $updkas = $kas->updKas($nm_kas,$id_tipe_kas,$id_branch,$no_rek,$pemilik_rek,$id_kas);
            echo $updkas;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}