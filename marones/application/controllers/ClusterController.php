<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class ClusterController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Cluster');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst="active";
        $this->view->active_cluster="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Cluster";
        // navigation
        $this->_helper->navbar(0,0,'cluster/new',0,0);
        $cluster= new cluster();
        $this->view->listCluster=$cluster->fetchAll();
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Cluster";
        // navigation
        $this->_helper->navbar(0,'cluster',0,0,0);
        // cluster
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $nm_cluster = $this->_helper->string->esc_quote(trim($request['nm']));
        $aktif= "t";
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_cluster,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama cluster tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $cluster = new cluster();
           $setcluster = $cluster->setcluster($nm_cluster);
            echo $setcluster;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $cluster = new cluster();
        $delcluster=$cluster->delcluster($id);
        echo $delcluster;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Cluster";
        // Navbar
        $this->_helper->navbar('cluster',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $cluster=new cluster();
        $getcluster=$cluster->getclusterById($id);
        if(!$getcluster){
            $this->view->eksis="f";
        }else{
            foreach ($getcluster as $data){
                $this->view->id=$data['id_cluster'];
                $this->view->nm=$data['nm_cluster'];
            }
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_cluster= $request['id'];
        $nm_cluster = $this->_helper->string->esc_quote(trim($request['nm']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_cluster,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama cluster tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $cluster = new cluster();
            $updcluster = $cluster->updcluster($nm_cluster,$id_cluster);
            echo $updcluster;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}