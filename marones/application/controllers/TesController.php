<?php
class TesController extends Zend_Controller_Action
{
    function init(){
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        
        Zend_Loader::loadClass('Jabfung');
        Zend_Loader::loadClass('Jabstruk');
        Zend_Loader::loadClass('Pegawai');
        Zend_Loader::loadClass('PegawaiJabfung');
        Zend_Loader::loadClass('PegawaiJabstruk');
        Zend_Loader::loadClass('PegawaiPangkat');
        Zend_Loader::loadClass('Sdm');
        Zend_Loader::loadClass('SdmOrganisasiProfesi');
        Zend_Loader::loadClass('SdmPelatihan');
        Zend_Loader::loadClass('SdmPendidikan');
        Zend_Loader::loadClass('SdmPenghargaan');
        Zend_Loader::loadClass('Sk');
        Zend_Loader::loadClass('UnitKerja');
        
        
        
        $this->_helper->layout()->disableLayout();
    }
    
    function indexAction ()
    {
     $this->view->x="sodikin";
     $jabstruk= new Jabstruk();
     $jabfung= new Jabfung();
     $pegawai= new Pegawai();
     $pegawaijabfung= new PegawaiJabfung();
     $pegawaijabstruk= new PegawaiJabstruk();
     $pegawaiPangkat= new PegawaiPangkat();
     $sdm= new Sdm();
     $sdmorganisasiprofesi= new SdmOrganisasiProfesi();
     $sdmpelatihan= new SdmPelatihan();
     $sdmpendidikan= new SdmPendidikan();
     $sdmpenghargaan= new SdmPenghargaan();
     $sk= new Sk();
     $unitkerja= new UnitKerja();
//      $nm_jabfung = "dosen"; 
//      $id_unit = "1"; 
//      $aktif= "t";
//      $skor= 10;
//      $set = $jabfung->setJabfung($nm_jabfung, $id_unit, $aktif, $skor);
//      $set2= $jabstruk->setJabstruk("dosen", "1", "t",10);
//      $set3= $pegawai->setPegawai("1", "2018-01-01", "1", "12121", "1", "1", "12312", "4634364");
//      $set4= $pegawaijabfung->setPegawaiJabfung("025c26d2-a678-9298-7616-a394d6c163fb", "1", "1", "aa", "as", "2018-01-01");
//      $set5= $pegawaijabstruk->setPegawaiJabstruk("025c26d2-a678-9298-7616-a394d6c163fb", "c4b922c8-8029-ed93-57c8-941a78d52b6a", "1", "aaa", "aas", "2018-01-01");
//      $set6= $pegawaiPangkat->setPegawaiPangkat("025c26d2-a678-9298-7616-a394d6c163fb", "1", "1", "aa", "as", "2018-01-01");
//      $set7= $sdm->setSdm("aab", "q", "2000-01-01", "l", "1", "1", "2343342432", "asaaaaa", "asaaaa");
//      $set8= $sdmorganisasiprofesi->setSdmOrganisasiProfesi("1", "aa", "a","0");
//         $set9= $sdmpelatihan->setSdmPelatihan("1", "q",2000, 0);
//         $set10= $sdmpendidikan->setSdmPendidikan("1", "1","aa", "as", 2001, 0);
//         $set11= $sdmpenghargaan->setSdmPenghargaan("1", "as", "asa",2018, 0);
//         $set12 = $sk->setSk("43", "2018-01-01", "aa", "1", "1","aaa");
//         $set13 = $unitkerja->setUnitKerja("aaa", "t");
     $this->view->hasil=$set13;
    }
}
?>