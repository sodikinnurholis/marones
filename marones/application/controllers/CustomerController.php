<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class CustomerController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Customer');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst="active";
        $this->view->active_customer="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Customer";
        // navigation
        $this->_helper->navbar(0,0,'customer/new',0,0);
        $customer= new Customer();
        $this->view->listCustomer=$customer->fetchAll();
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Customer";
        // navigation
        $this->_helper->navbar(0,'customer',0,0,0);
        // customer
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $nm_customer = $this->_helper->string->esc_quote(trim($request['nm']));
        $nik = $this->_helper->string->esc_quote(trim($request['nik']));
        $kontak = $this->_helper->string->esc_quote(trim($request['kontak']));
        $email = $this->_helper->string->esc_quote(trim($request['email']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_customer,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama customer tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($nik,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- NIK tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Kontak tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
         if($vd->validasiLength($email,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Email tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $customer = new Customer();
           $setCustomer = $customer->setCustomer($nm_customer,$nik,$kontak,$email);
            echo $setCustomer;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $customer = new Customer();
        $delCustomer=$customer->delCustomer($id);
        echo $delCustomer;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Customer";
        // Navbar
        $this->_helper->navbar('customer',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $customer=new Customer();
        $getCustomer=$customer->getCustomerById($id);
        if(!$getCustomer){
            $this->view->eksis="f";
        }else{
            foreach ($getCustomer as $data){
                $this->view->id=$data['id_customer'];
                $this->view->nm=$data['nm_customer'];
                $this->view->nik=$data['nik'];
                $this->view->kontak=$data['kontak'];
                $this->view->email=$data['email'];
            }
        } 
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id= $request['id'];
        $nm_customer = $this->_helper->string->esc_quote(trim($request['nm']));
        $nik = $this->_helper->string->esc_quote(trim($request['nik']));
        $kontak = $this->_helper->string->esc_quote(trim($request['kontak']));
        $email = $this->_helper->string->esc_quote(trim($request['email']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_customer,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama customer tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($nik,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- NIK tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Kontak tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
         if($vd->validasiLength($email,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Email tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $customer = new Customer();
            $updCustomer = $customer->updCustomer($nm_customer,$nik,$kontak,$email,$id);
            echo $updCustomer;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}