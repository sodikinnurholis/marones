<?php
/*
	Programmer	: Tiar Aristian
	Release		: Januari 2016
	Module		: Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class ItemController extends MasterController
{
	function init()
	{
	    parent::init();
		$this->initView();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('Item');
		Zend_Loader::loadClass('Zend_Session');
		Zend_Loader::loadClass('Zend_Layout');
		// layout
		$this->_helper->layout()->setLayout('main');
		// treeview
		$this->view->active_mst="active";
		$this->view->active_item="active";
		
	}

	function indexAction()
	{
		// Title Browser
		$this->view->title = "Item";
		// navigation
		$this->_helper->navbar(0,0,'item/new',0,0);
		$item= new Item();
		$this->view->listItem=$item->fetchAll();
	}
	
	function newAction()
	{
	  
	    // Title Browser
	    $this->view->title = "Input Data Item";
        // navigation
        $this->_helper->navbar(0,'item',0,0,0);
	    // item kerja
	   
	}
	
	function ainsAction(){
	    // disable layout
	    $this->_helper->layout->disableLayout();
	    // start inserting
	    $request = $this->getRequest()->getPost();
	    $nm_item = $this->_helper->string->esc_quote(trim($request['nm']));
	    $kd_item = $this->_helper->string->esc_quote(trim($request['kd']));
	    $keterangan = $this->_helper->string->esc_quote(trim($request['keterangan']));
	    $is_group= "f";
	    $id_unit_def = $this->_helper->string->esc_quote(trim($request['id_unit']));
	    $id_item_class = $this->_helper->string->esc_quote(trim($request['id_item_class']));
	    // validation
	    $err=0;
	    $msg="";
	    $vd = new Validation();
	    if($vd->validasiLength($nm_item,1,200)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama item tidak boleh kosong maksimal 200 huruf</strong><br>";
	    }
	    if($vd->validasiLength($kd_item,1,200)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Kode item tidak boleh kosong maksimal 200 huruf</strong><br>";
	    }
	    if($vd->validasiLength($keterangan,1,200)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Keterangan tidak boleh kosong maksimal 200 huruf</strong><br>";
	    }
	     if($vd->validasiLength($id_unit_def,1,200)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama Unit tidak boleh kosong</strong><br>";
	    }
	    if($vd->validasiLength($id_item_class,1,200)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama Item Class tidak boleh kosong</strong><br>";
	    }
	    if($err==0){
	        $item = new Item();
	       $setItem = $item->setItem($nm_item,$kd_item,$id_item_class,$keterangan,$id_unit_def);
	        echo $setItem;
	    }else{
	        echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
	    }
	}
	
	function adelAction(){
	    // disabel layout
	    $this->_helper->layout->disableLayout();
	    // set database
	    $request = $this->getRequest()->getPost();
	    // gets value from ajax request
	    $param = $request['param'];
	    $id = $param[0];
	    $item = new Item();
	    $delItem=$item->delItem($id);
	    echo $delItem;
	}
	
	function editAction()
	{
	    // Title Browser
	    $this->view->title = "Edit Data Item";
	    // Navbar
	    $this->_helper->navbar('item',0,0,0,0);
	    // get data
	    $id=$this->_request->get('id');
	    $item=new Item();
	    $getItem=$item->getItemById($id);
	    if(!$getItem){
	        $this->view->eksis="f";
	    }else{
	        foreach ($getItem as $data){
	            $this->view->id=$data['id_item'];
	            $this->view->nm=$data['nm_item'];
	            $this->view->kd=$data['kd_item'];
	            $this->view->keterangan=$data['keterangan'];
	            $this->view->id_unit=$data['id_unit_def'];
	            $this->view->nm_unit=$data['nm_unit'];
	            $this->view->id_item_class=$data['id_item_class'];
	            $this->view->nm_class=$data['nm_class'];
	        }
	    }
	    
	}
	
	function aupdAction(){
	    // disable layout
	    $this->_helper->layout->disableLayout();
	    // start inserting
	    $request = $this->getRequest()->getPost();
	    $id_item= $request['id'];
	    $nm_item = $this->_helper->string->esc_quote(trim($request['nm']));
	    $kd_item = $this->_helper->string->esc_quote(trim($request['kd']));
	    $keterangan = $this->_helper->string->esc_quote(trim($request['keterangan']));
	    $is_group= "f";
	    $id_unit_def = $this->_helper->string->esc_quote(trim($request['id_unit']));
	    $id_item_class = $this->_helper->string->esc_quote(trim($request['id_item_class']));
	    // validation
	    $err=0;
	    $msg="";
	    $vd = new Validation();
	    if($vd->validasiLength($nm_item,1,200)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama item tidak boleh kosong maksimal 200 huruf</strong><br>";
	    }
	    if($vd->validasiLength($kd_item,1,200)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Kode item tidak boleh kosong maksimal 200 huruf</strong><br>";
	    }
	    if($vd->validasiLength($keterangan,1,200)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Keterangan tidak boleh kosong maksimal 200 huruf</strong><br>";
	    }
	     if($vd->validasiLength($id_unit_def,1,200)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama Unit tidak boleh kosong</strong><br>";
	    }
	    if($vd->validasiLength($id_item_class,1,200)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama item class tidak boleh kosong</strong><br>";
	    }
	    if($err==0){
	        $item = new Item();
	        $updItem = $item->updItem($nm_item,$kd_item,$id_item_class,$keterangan,$id_unit_def,$id_item);
	        echo $updItem;
	    }else{
	        echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
	    }
	}
	
}