<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class WorkflowController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Workflow');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst="active";
        $this->view->active_workflow="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Workflow";
        // navigation
        $this->_helper->navbar(0,0,'workflow/new',0,0);
        $workflow= new Workflow();
        $this->view->listWorkflow=$workflow->fetchAll();
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Workflow";
        // navigation
        $this->_helper->navbar(0,'workflow',0,0,0);
        // workflow
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $nm_workflow = $this->_helper->string->esc_quote(trim($request['nm']));
        $aktif= "t";
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_workflow,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama workflow tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $workflow = new Workflow();
           $setWorkflow = $workflow->setWorkflow($nm_workflow);
            echo $setWorkflow;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $workflow = new Workflow();
        $delWorkflow=$workflow->delWorkflow($id);
        echo $delWorkflow;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Workflow";
        // Navbar
        $this->_helper->navbar('Workflow',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $workflow=new Workflow();
        $getWorkflow=$workflow->getWorkflowById($id);
        if(!$getWorkflow){
            $this->view->eksis="f";
        }else{
            foreach ($getWorkflow as $data){
                $this->view->id=$data['id_workflow'];
                $this->view->nm=$data['nm_workflow'];
            }
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_workflow= $request['id'];
        $nm_workflow = $this->_helper->string->esc_quote(trim($request['nm']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_workflow,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama workflow tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $workflow = new Workflow();
            $updWorkflow = $workflow->updWorkflow($nm_workflow,$id_workflow);
            echo $updWorkflow;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}