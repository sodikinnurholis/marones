<?php
/*
	Programmer	: Tiar Aristian
	Release		: Januari 2016
	Module		: Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class UnitController extends MasterController
{
	function init()
	{
	    parent::init();
		$this->initView();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('Unit');
		Zend_Loader::loadClass('Zend_Session');
		Zend_Loader::loadClass('Zend_Layout');
		// layout
		$this->_helper->layout()->setLayout('main');
		// treeview
		$this->view->active_mst="active";
		$this->view->active_unit="active";
		
	}

	function indexAction()
	{
		// Title Browser
		$this->view->title = "Unit Kerja";
		// navigation
		$this->_helper->navbar(0,0,'unit/new',0,0);
		$unit= new Unit();
		$this->view->listUnit=$unit->fetchAll();
	}
	
	function newAction()
	{
	  
	    // Title Browser
	    $this->view->title = "Input Data Unit Kerja";
        // navigation
        $this->_helper->navbar(0,'unit',0,0,0);
	    // unit kerja
	   
	}
	
	function ainsAction(){
	    // disable layout
	    $this->_helper->layout->disableLayout();
	    // start inserting
	    $request = $this->getRequest()->getPost();
	    $nm_unit = $this->_helper->string->esc_quote(trim($request['nm']));
	    $id_kategori_unit = $this->_helper->string->esc_quote(trim($request['kategori']));
	    $aktif= "t";
	    // validation
	    $err=0;
	    $msg="";
	    $vd = new Validation();
	    if($vd->validasiLength($nm_unit,1,50)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama unit tidak boleh kosong maksimal 50 huruf</strong><br>";
	    }
	    if($vd->validasiLength($id_kategori_unit,1,50)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Kategori unit tidak boleh kosong maksimal 50 huruf</strong><br>";
	    }
	    if($err==0){
	        $unit = new Unit();
	       $setUnit = $unit->setUnit($nm_unit);
	        echo $setUnit;
	    }else{
	        echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
	    }
	}
	
	function adelAction(){
	    // disabel layout
	    $this->_helper->layout->disableLayout();
	    // set database
	    $request = $this->getRequest()->getPost();
	    // gets value from ajax request
	    $param = $request['param'];
	    $id = $param[0];
	    $unit = new Unit();
	    $delUnit=$unit->delUnit($id);
	    echo $delUnit;
	}
	
	function editAction()
	{
	    // Title Browser
	    $this->view->title = "Edit Data Unit Kerja";
	    // Navbar
	    $this->_helper->navbar('unit',0,0,0,0);
	    // get data
	    $id=$this->_request->get('id');
	    $unit=new Unit();
	    $getUnit=$unit->getUnitById($id);
	    if(!$getUnit){
	        $this->view->eksis="f";
	    }else{
	        foreach ($getUnit as $data){
	            $this->view->id=$data['id_unit'];
	            $this->view->nm=$data['nm_unit'];
	            $this->view->kategori=$data['nm_kategori_unit'];
	        }
	    }
	    
	}
	
	function aupdAction(){
	    // disable layout
	    $this->_helper->layout->disableLayout();
	    // start inserting
	    $request = $this->getRequest()->getPost();
	    $id_unit= $request['id'];
	    $nm_unit = $this->_helper->string->esc_quote(trim($request['nm']));
	    $id_kategori_unit = $this->_helper->string->esc_quote(trim($request['kategori']));
	    // validation
	    $err=0;
	    $msg="";
	    $vd = new Validation();
	    if($vd->validasiLength($nm_unit,1,50)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama unit tidak boleh kosong maksimal 50 huruf</strong><br>";
	    }
	    if($vd->validasiLength($id_kategori_unit,1,50)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Kategori unit tidak boleh kosong maksimal 50 huruf</strong><br>";
	    }
	    if($err==0){
	        $unit = new Unit();
	        $updUnit = $unit->updUnit($nm_unit,$id_kategori_unit,$id_unit);
	        echo $updUnit;
	    }else{
	        echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
	    }
	}
	
}