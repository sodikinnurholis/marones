<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class PoController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Po');
        Zend_Loader::loadClass('PoItem');
        Zend_Loader::loadClass('Item');
        Zend_Loader::loadClass('Unitkonv');
        Zend_Loader::loadClass('Supplier');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');  
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_trs="active";
        $this->view->active_po="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Po";
        // navigation
        $this->_helper->navbar(0,0,'po/new',0,0);
        // destroy session param
        Zend_Session::namespaceUnset('par_po');
        $supplier=new Supplier();
        $this->view->listSupplier=$supplier->fetchAll();
    }

    function ashowAction(){
        // makes disable layout
        $this->_helper->getHelper('layout')->disableLayout();
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $supplier=$request['supplier'];
        $closed=$request['status'];
        $tgl1=$request['tgl1'];
        $tgl2=$request['tgl2'];
        // set session
        $param = new Zend_Session_Namespace('par_po');
        $param->supplier=$supplier;
        $param->closed=$closed;
        $param->tgl1=$tgl1;
        $param->tgl2=$tgl2;
    }
    function listAction(){
        // Title Browser
        $this->view->title = "Daftar PO";
        // Navbar
        $this->_helper->navbar('po',0,'po/new',0,0);
        // collapse
        $this->view->sidecolaps="sidebar-collapse";
        // get param
        $param = new Zend_Session_Namespace('par_po');
        $supplier=$param->supplier;
        $closed=$param->closed;
        $tgl1=$param->tgl1;
        $tgl2=$param->tgl2;
        // get data
        $po=new Po();
        $getPo=$po->queryPo($supplier,$closed, $tgl1, $tgl2);
        $this->view->listPo=$getPo;
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data PO";
        // navigation
        $this->_helper->navbar(0,'po',0,0,0);
        // po
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $tgl_po = $request['tgl_po'];
        $keterangan = $this->_helper->string->esc_quote(trim($request['keterangan']));
        $id_supplier = $this->_helper->string->esc_quote(trim($request['id_supplier']));
        $vat_in = $request['vat_in'];
        $include_vat = "f";
        $diskon_po = $request['diskon_po'];
        $est_freight = $request['est_freight'];
        $closed = "F";
        $id_fob = $this->_helper->string->esc_quote(trim($request['id_fob']));
        $id_termin = $this->_helper->string->esc_quote(trim($request['id_termin']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        
        if($vd->validasiLength($tgl_po,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Tanggal po tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($keterangan,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Keterangan tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_supplier,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- ID supplier tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
         if($vd->validasiBetween($vat_in,0,10)=='F'){
            $err++;
            $msg=$msg."<strong>- Vat In tidak boleh kosong</strong><br>";
        }
        if($vd->validasiBetween($diskon_po,1,999999999999999)=='F'){
            $err++;
            $msg=$msg."<strong>- diskon harus lebih dari 0</strong><br>";
        }
        if($vd->validasiBetween($est_freight,1,10)=='F'){
            $err++;
            $msg=$msg."<strong>- est freight harus lebih dari 0</strong><br>";
        }
        if($vd->validasiLength($id_fob,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- FOB tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_termin,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Termin tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $po = new Po();
           $setPo = $po->setPo($tgl_po,$keterangan,$id_supplier,$vat_in,$include_vat,$diskon_po,$est_freight,$closed,$id_fob,$id_termin);
            echo $setPo;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $po = new Po();
        $delPo=$po->delPo($id);
        echo $delPo;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Po";
        // Navbar
        $this->_helper->navbar('po',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $po=new Po();
        $getPo=$po->getPoById($id);
        if(!$getPo){
            $this->view->eksis="f";
        }else{
            foreach ($getPo as $data){
                $this->view->id_po=$data['id_po'];
                $no_po=$data['no_po'];
                $this->view->tgl_po=$data['tgl_po_fmt'];
                $this->view->keterangan=$data['keterangan'];
                $this->view->id_supplier=$data['id_supplier'];
                $this->view->nm_supplier=$data['nm_supplier'];
                $this->view->vat_in=$data['vat_in'];
                $this->view->id_fob=$data['id_fob'];
                $this->view->include_vat=$data['include_vat'];
                $this->view->diskon_po=$data['diskon_po'];
                $this->view->est_freight=$data['est_freight'];
                $this->view->closed=$data['closed'];
                $this->view->id_termin=$data['id_termin'];
            }
            $this->view->title = "Edit Data Po ".$no_po;
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id= $request['id'];
        $tgl_po = $request['tgl_po'];
        $keterangan = $this->_helper->string->esc_quote(trim($request['keterangan']));
        $id_supplier = $this->_helper->string->esc_quote(trim($request['id_supplier']));
        $vat_in = $request['vat_in'];
        $include_vat = "f";
        $diskon_po = $request['diskon_po'];
        $est_freight = $request['est_freight'];
        $closed = "f";
        $id_fob = $this->_helper->string->esc_quote(trim($request['id_fob']));
        $id_termin = $this->_helper->string->esc_quote(trim($request['id_termin']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        
        if($vd->validasiLength($tgl_po,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Tanggal so tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($keterangan,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Keterangan tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_supplier,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- ID supplier tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiBetween($vat_in,0,999999999999999)=='F'){
            $err++;
            $msg=$msg."<strong>- Vat in harus lebih dari 0</strong><br>";
        }
        if($vd->validasiBetween($diskon_po,1,999999999999999)=='F'){
            $err++;
            $msg=$msg."<strong>- diskon po harus lebih dari 0</strong><br>";
        }
        if($vd->validasiBetween($est_freight,1,999999999999999)=='F'){
            $err++;
            $msg=$msg."<strong>- est freight harus lebih dari 0</strong><br>";
        }
        if($vd->validasiLength($id_fob,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- FOB tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_termin,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Termin tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $po = new Po();

            $updPo = $po->updPo($tgl_po,$keterangan,$id_supplier,$vat_in,$include_vat,$diskon_po,$est_freight,$closed,$id_fob,$id_termin,$id);
            echo $updPo;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }

    function new2Action(){
        // Title Browser
        $this->view->title = "Detil Pemesanan Item ";
        // collapse
        $this->view->sidecolaps="sidebar-collapse";
        // get data
        $id=$this->_request->get('id');
        $bb=$this->_request->get('bb');
        $tab=$this->_request->get('tab');
        // Navbar
        if($bb=='l'){
            $this->_helper->navbar('po/list',0,'po/new','po/export',0);
        }else{
            $this->_helper->navbar('po',0,'po/new','po/export',0);
        }
        $this->view->bb=$bb;
        $po=new Po();
        $getPo=$po->getPoById($id);
        if(!$getPo){
            $this->view->eksis="f";
            $this->_helper->navbar('po',0,'po/new',0,0);
        }else{
            foreach ($getPo as $data){
                $this->view->id=$data['id_po'];
                $this->view->kd=$data['no_po'];
                $this->view->tgl=$data['tgl_po_fmt'];
                $this->view->ket=$data['keterangan'];
                $this->view->supp=$data['id_supplier'];
                $this->view->nmsupp=$data['nm_supplier'];
                if($data['vat_in']>0){
                    $this->view->selVatY="selected='selected'";
                    $this->view->vat="YA";
                }else{
                    $this->view->selVatT="selected='selected'";
                    $this->view->vat="TIDAK";
                }
                $this->view->vat_val=$data['vat_in'];
                $this->view->disc=$data['diskon_po'];
                $this->view->freight=$data['est_freight'];
                $this->view->closed=$data['closed'];
                if($data['closed']=='f'){
                    $this->view->stat="OPEN";
                    $this->view->warna="danger";
                }else{
                    $this->view->stat="CLOSED";
                    $this->view->warna="success";
                }
               
                $tgl=$data['tgl_po_fmt'];
                $vat=$data['vat_in'];
                $disc=$data['diskon_po'];
                $freight=$data['est_freight'];
            }
            // tab
            if($tab=='itm'){
                $this->view->dItm="disabled='disabled'";
                $this->view->page="_item.phtml";
            }else{
                $this->view->dOview="disabled='disabled'";
                $this->view->page="_overview.phtml";
            }
            // get data PO Item
            $poItem=new PoItem();
            $getPoItem=$poItem->getPoItemtByPo($id);
            $this->view->listPoItem=$getPoItem;
            // create session for export
            $param = new Zend_Session_Namespace('par_epo');
           
           
            $param->tgl=$tgl;
            
            $param->vat_in_item=$vat;
            
            $param->idpo=$getPoItem;
        }
    }
    function ains2Action(){
        // disabel layout
        $this->_helper->layout->disableLayout();
    
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_po=$request['id'];
        $id_item = $this->_helper->string->esc_quote(trim($request['id_item']));
        $qty=floatval($request['qty']);
        $sat = $this->_helper->string->esc_quote(trim($request['sat']));
        $price=floatval($request['price']);
        $vat=0;
        $item=new Item();
        $getItem=$item->getItemById($id_item);
        $sat_def=0;
        foreach ($getItem as $dtItem){
            $sat_def=$dtItem['id_unit_def'];
        }
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if(($vd->validasiLength($id_item,1,100)=='F')){
            $err++;
            $msg=$msg."<strong>- Data item tidak boleh kosong</strong><br>";
        }
        if(($vd->validasiBetween($qty, 0.01, 999999999999999)=='F')){
            $err++;
            $msg=$msg."<strong>- Quantity Beli harus lebih dari 0</strong><br>";
        }
        if(($vd->validasiLength($sat,1,50)=='F')){
            $err++;
            $msg=$msg."<strong>- Satuan beli tidak boleh kosong</strong><br>";
        }
        if($sat==$sat_def){
            $konv=1;
        }else{
            $unitKonv=new UnitKonv();
            $getKonv=$unitKonv->getUnitkonvById($sat_def,$sat);
            if(!$getKonv){
                $err++;
                $msg=$msg."<strong>- Konversi dari unit beli ke unit default tidak ditemukan. Silakan buat dahulu konversinya</strong><br>";
            }else{
                foreach ($getKonv as $dtKonv){
                    $konv=$dtKonv['konv'];
                }
            }
        }
        if(($vd->validasiBetween($price, 0.01, 999999999999999)=='F')){
            $err++;
            $msg=$msg."<strong>- Harga satuan harus lebih dari 0</strong><br>";
        }
        if($err==0){
            $poItem = new PoItem();
            $setPoItem=$poItem->setPoItem($id_po, $id_item, $qty, $price, $sat, $konv, $vat);
            echo $setPoItem;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
            
    }
    function adel2Action(){
        // disabel layout
        $this->_helper->layout->disableLayout();
       
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $poItem = new PoItem();
        $delPoItem=$poItem->delPoItem($id);
        echo $delPoItem;
    }
    function edit2Action(){
            // cek akses aksi
          
                // Title Browser
                $this->view->title = "Edit Data Detil Pemesanan Item";
                // get data
                $id=$this->_request->get('id');
            $bb=$this->_request->get('bb');
            $this->view->bb=$bb;
                $poItem=new PoItem();
                $getPoItem=$poItem->getPoItemtById($id);
                if(!$getPoItem){
                    $this->view->eksis="f";
                    // Navbar
                    $this->_helper->navbar('po',0,0,0,0);
                }else{
                    foreach ($getPoItem as $data){
                        $this->view->id=$data['id_po_item'];
                        $this->view->id_po=$data['id_po'];
                        $id=$data['id_po'];
                    $closed=$data['closed'];
                    $this->view->id_item=$data['id_item'];
                        $this->view->nm_item=$data['nm_item'];
                        $this->view->kd_item=$data['kd_item'];
                        $this->view->unit_def=$data['nm_unit_def'];
                        $this->view->qty_po=$data['qty_po'];
                    $this->view->id_unit_po=$data['id_unit_beli'];
                    $this->view->nm_unit_po=$data['nm_unit_beli'];
                    $this->view->price_beli=$data['price_beli'];
                    }
                    // Navbar
                    $this->_helper->navbar('po/new2?id='.$id.'&bb='.$bb.'&tab=itm',0,0,0,0);
                // ref
                    
                    if($closed=='t'){
                        $this->view->eksis="f";
                            // Navbar
                            $this->_helper->navbar('po',0,0,0,0);
                    }
                }
            
        }

    function aupd2Action(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id=$request['id'];
         $id_po=$request['id_po'];
        $id_item = $this->_helper->string->esc_quote(trim($request['id_item']));
        $qty=floatval($request['qty']);
        $sat = $this->_helper->string->esc_quote(trim($request['sat']));
        $price=floatval($request['price']);
        $vat=0;
        if($id_item==''){
            $id_item=0;
        }
        if($sat==''){
            $sat=0;
        }
        $item=new Item();
        $getItem=$item->getItemById($id_item);
        $sat_def=0;
        foreach ($getItem as $dtItem){
            $sat_def=$dtItem['id_unit_def'];
        }
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if(($vd->validasiLength($id_item,1,100)=='F')){
            $err++;
            $msg=$msg."<strong>- Data item tidak boleh kosong</strong><br>";
        }
        if(($vd->validasiBetween($qty, 0.01, 999999999999999)=='F')){
            $err++;
            $msg=$msg."<strong>- Quantity Beli harus lebih dari 0</strong><br>";
        }
        if(($vd->validasiLength($sat,1,50)=='F')){
            $err++;
            $msg=$msg."<strong>- Satuan beli tidak boleh kosong</strong><br>";
        }
        if($sat==$sat_def){
            $konv=1;
        }else{
            $unitKonv=new Unitkonv();
            $getKonv=$unitKonv->getUnitkonvById($sat_def,$sat);
            if(!$getKonv){
                $err++;
                $msg=$msg."<strong>- Konversi dari unit beli ke unit default tidak ditemukan. Silakan buat dahulu konversinya</strong><br>";
            }else{
                foreach ($getKonv as $dtKonv){
                    $konv=$dtKonv['konv'];
                }
            }
        }
        if(($vd->validasiBetween($price, 0.01, 999999999999999)=='F')){
            $err++;
            $msg=$msg."<strong>- Harga satuan harus lebih dari 0</strong><br>";
        }
        if($err==0){
            $poItem = new PoItem();
            $updPoItem=$poItem->updPoItem($id_po, $id_item, $qty, $price, $sat, $konv, $vat,$id);
            echo $updPoItem;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
        
    
}
