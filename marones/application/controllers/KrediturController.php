<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class KrediturController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Kreditur');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst_kontak="active";
        $this->view->active_kreditur="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Kreditur";
        // navigation
        $this->_helper->navbar(0,0,'kreditur/new',0,0);
        $kreditur= new Kreditur();
        $this->view->listKreditur=$kreditur->fetchAll();

    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Kreditur";
        // navigation
        $this->_helper->navbar(0,'kreditur',0,0,0);
        // kreditur
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_kontak = $this->_helper->string->esc_quote(trim($request['id_kontak']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($id_kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama kreditur tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
       
        if($err==0){
            $kreditur = new Kreditur();
           $setkreditur = $kreditur->setKreditur($id_kontak);
            echo $setkreditur;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $kreditur = new Kreditur();
        $delkreditur=$kreditur->delKreditur($id);
        echo $delkreditur;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Kreditur";
        // Navbar
        $this->_helper->navbar('kreditur',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $kreditur=new Kreditur();
        $getkreditur=$kreditur->getKrediturById($id);
        if(!$getkreditur){
            $this->view->eksis="f";
        }else{
            foreach ($getkreditur as $data){
                $this->view->id=$data['id_kreditur'];
                $this->view->nm_debit_tur=$data['nm_debit_tur'];
                $this->view->id_kontak=$data['id_kontak'];
            }
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_kreditur= $request['id'];
        $id_kontak = $this->_helper->string->esc_quote(trim($request['id_kontak']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($id_kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama kreditur tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $kreditur = new Kreditur();
            $updkreditur = $kreditur->updKreditur($id_kontak,$id_kreditur);
            echo $updkreditur;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}