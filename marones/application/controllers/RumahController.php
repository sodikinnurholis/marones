<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class RumahController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Rumah');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst="active";
        $this->view->active_rumah="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Rumah";
        // navigation
        $this->_helper->navbar(0,0,'rumah/new',0,0);
        $rumah= new Rumah();
        $this->view->listRumah=$rumah->fetchAll();
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Rumah";
        // navigation
        $this->_helper->navbar(0,'rumah',0,0,0);
        // rumah
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $nm_rumah = $this->_helper->string->esc_quote(trim($request['nm']));
        $no_rumah = $this->_helper->string->esc_quote(trim($request['no']));
        $harga = $this->_helper->string->esc_quote(trim($request['harga']));
        $id_cluster = $this->_helper->string->esc_quote(trim($request['id_cluster']));
        $aktif= "t";
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_rumah,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama rumah tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($no_rumah,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nomer rumah tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($harga,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- harga rumah tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_cluster,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- id cluster tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $rumah = new rumah();
           $setrumah = $rumah->setRumah($nm_rumah,$no_rumah,$harga,$id_cluster);
            echo $setrumah;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $rumah = new Rumah();
        $delrumah=$rumah->delRumah($id);
        echo $delrumah;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Rumah";
        // Navbar
        $this->_helper->navbar('rumah',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $rumah=new Rumah();
        $getrumah=$rumah->getRumahById($id);
        if(!$getrumah){
            $this->view->eksis="f";
        }else{
            foreach ($getrumah as $data){
                $this->view->id=$data['id_rumah'];
                $this->view->nm=$data['nm_rumah'];
                $this->view->no=$data['no_rumah'];
                $this->view->harga=$data['harga'];
                $this->view->id_cluster=$data['id_cluster'];
                $this->view->nm_cluster=$data['nm_cluster'];
            }
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_rumah= $request['id'];
        $nm_rumah = $this->_helper->string->esc_quote(trim($request['nm']));
        $no_rumah = $this->_helper->string->esc_quote(trim($request['no']));
        $harga = $this->_helper->string->esc_quote(trim($request['harga']));
        $id_cluster = $this->_helper->string->esc_quote(trim($request['id_cluster']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_rumah,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama rumah tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($no_rumah,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nomer rumah tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($harga,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- harga rumah tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_cluster,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- id cluster tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $rumah = new Rumah();
            $updrumah = $rumah->updRumah($nm_rumah,$no_rumah,$harga,$id_cluster,$id_rumah);
            echo $updrumah;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}