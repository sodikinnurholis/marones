<?php
/*
	Programmer	: Tiar Aristian
	Release		: Januari 2016
	Module		: Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class SatuanController extends MasterController
{
	function init()
	{
	    parent::init();
		$this->initView();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('Satuan');
		Zend_Loader::loadClass('KategoriUnit');
		Zend_Loader::loadClass('Zend_Session');
		Zend_Loader::loadClass('Zend_Layout');
		// layout
		$this->_helper->layout()->setLayout('main');
		// treeview
		$this->view->active_mst="active";
		$this->view->active_satuan="active";
		
	}

	function indexAction()
	{
		// Title Browser
		$this->view->title = "Satuan";
		// navigation
		$this->_helper->navbar(0,0,'satuan/new',0,0);
		$satuan= new Satuan();
		$this->view->listSatuan=$satuan->fetchAll();
		$kategoriunit= new KategoriUnit();
        $this->view->listKategoriUnit=$kategoriunit->fetchAll();
	}
	
	function newAction()
	{
	  
	    // Title Browser
	    $this->view->title = "Input Data Satuan";
        // navigation
        $this->_helper->navbar(0,'satuan',0,0,0);
	    // satuan kerja
	   
	}
	
	function ainsAction(){
	    // disable layout
	    $this->_helper->layout->disableLayout();
	    // start inserting
	    $request = $this->getRequest()->getPost();
	    $nm_satuan = $this->_helper->string->esc_quote(trim($request['nm']));
	    $id_kategori_unit = $this->_helper->string->esc_quote(trim($request['kategori']));
	    $aktif= "t";
	    // validation
	    $err=0;
	    $msg="";
	    $vd = new Validation();
	    if($vd->validasiLength($nm_satuan,1,50)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama satuan tidak boleh kosong maksimal 50 huruf</strong><br>";
	    }
	    if($vd->validasiLength($id_kategori_unit,1,200)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama Kategori Unit tidak boleh kosong</strong><br>";
	    }
	    if($err==0){
	        $satuan = new Satuan();
	       $setSatuan = $satuan->setSatuan($nm_satuan,$id_kategori_unit);
	        echo $setSatuan;
	    }else{
	        echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
	    }
	}
	
	function adelAction(){
	    // disabel layout
	    $this->_helper->layout->disableLayout();
	    // set database
	    $request = $this->getRequest()->getPost();
	    // gets value from ajax request
	    $param = $request['param'];
	    $id = $param[0];
	    $satuan = new Satuan();
	    $delSatuan=$satuan->delSatuan($id);
	    echo $delSatuan;
	}
	
	function editAction()
	{
	    // Title Browser
	    $this->view->title = "Edit Data Satuan";
	    // Navbar
	    $this->_helper->navbar('satuan',0,0,0,0);
	    // get data
	    $id=$this->_request->get('id');
	    $satuan=new Satuan();
	    $getSatuan=$satuan->getSatuanById($id);
	    if(!$getSatuan){
	        $this->view->eksis="f";
	    }else{
	        foreach ($getSatuan as $data){
	            $this->view->id=$data['id_unit'];
	            $this->view->nm=$data['nm_unit'];
	            $this->view->id_kategori_unit=$data['id_kategori_unit'];
	            $this->view->nm_kategori_unit=$data['nm_kategori_unit'];
	        }
	    }
	    
	}
	
	function aupdAction(){
	    // disable layout
	    $this->_helper->layout->disableLayout();
	    // start inserting
	    $request = $this->getRequest()->getPost();
	    $id_satuan= $request['id'];
	    $nm_satuan = $this->_helper->string->esc_quote(trim($request['nm']));
	    $id_kategori_unit = $this->_helper->string->esc_quote(trim($request['kategori']));
	    // validation
	    $err=0;
	    $msg="";
	    $vd = new Validation();
	    if($vd->validasiLength($nm_satuan,1,50)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama satuan tidak boleh kosong maksimal 50 huruf</strong><br>";
	    }
	    if($vd->validasiLength($id_kategori_unit,1,50)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama Kategori Unit tidak boleh kosong maksimal 50 huruf</strong><br>";
	    }
	    if($err==0){
	        $satuan = new Satuan();
	        $updSatuan = $satuan->updSatuan($nm_satuan,$id_kategori_unit,$id_satuan);
	        echo $updSatuan;
	    }else{
	        echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
	    }
	}
	
}