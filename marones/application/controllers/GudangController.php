<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class GudangController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Gudang');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst="active";
        $this->view->active_gudang="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Gudang";
        // navigation
        $this->_helper->navbar(0,0,'gudang/new',0,0);
        $gudang= new gudang();
        $this->view->listGudang=$gudang->fetchAll();
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Gudang";
        // navigation
        $this->_helper->navbar(0,'gudang',0,0,0);
        // gudang
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $nm_gudang = $this->_helper->string->esc_quote(trim($request['nm']));
        $aktif= "t";
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_gudang,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama gudang tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $gudang = new gudang();
           $setgudang = $gudang->setgudang($nm_gudang);
            echo $setgudang;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $gudang = new gudang();
        $delgudang=$gudang->delgudang($id);
        echo $delgudang;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Gudang";
        // Navbar
        $this->_helper->navbar('gudang',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $gudang=new gudang();
        $getgudang=$gudang->getgudangById($id);
        if(!$getgudang){
            $this->view->eksis="f";
        }else{
            foreach ($getgudang as $data){
                $this->view->id=$data['id_gudang'];
                $this->view->nm=$data['nm_gudang'];
            }
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_gudang= $request['id'];
        $nm_gudang = $this->_helper->string->esc_quote(trim($request['nm']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_gudang,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama gudang tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $gudang = new gudang();
            $updgudang = $gudang->updgudang($nm_gudang,$id_gudang);
            echo $updgudang;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}