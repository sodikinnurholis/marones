<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class SupplierController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Supplier');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst_kontak="active";
        $this->view->active_supplier="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Supplier";
        // navigation
        $this->_helper->navbar(0,0,'supplier/new',0,0);
        $supplier= new Supplier();
        $this->view->listSupplier=$supplier->fetchAll();
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Supplier";
        // navigation
        $this->_helper->navbar(0,'supplier',0,0,0);
        // supplier
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_kontak = $this->_helper->string->esc_quote(trim($request['id_kontak']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($id_kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama supplier tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
       
        if($err==0){
            $supplier = new Supplier();
           $setSupplier = $supplier->setSupplier($id_kontak);
            echo $setSupplier;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $supplier = new Supplier();
        $delSupplier=$supplier->delSupplier($id);
        echo $delSupplier;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Supplier";
        // Navbar
        $this->_helper->navbar('supplier',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $supplier=new Supplier();
        $getSupplier=$supplier->getSupplierById($id);
        if(!$getSupplier){
            $this->view->eksis="f";
        }else{
            foreach ($getSupplier as $data){
                $this->view->id=$data['id_supplier'];
                $this->view->id_kontak=$data['id_kontak'];
                $this->view->nm_supplier=$data['nm_supplier'];
            }
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_supplier= $request['id'];
        $id_kontak = $this->_helper->string->esc_quote(trim($request['id_kontak']));
       
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($id_kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama supplier tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        
        if($err==0){
            $supplier = new Supplier();
            $updsupplier = $supplier->updSupplier($id_kontak,$id_supplier);
            echo $updsupplier;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}
