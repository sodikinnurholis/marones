<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class SoController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('So');
        Zend_Loader::loadClass('Customer');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_trs="active";
        $this->view->active_so="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "So";
        // navigation
        $this->_helper->navbar(0,0,'so/new',0,0);
        $customer= new Customer();
        $this->view->listCus=$customer->fetchAll();
    }

    function ashowAction(){
        // makes disable layout
        $this->_helper->getHelper('layout')->disableLayout();
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $id_customer=$request['id_customer'];
        $closed=$request['status'];
        $tgl1=$request['tgl1'];
        $tgl2=$request['tgl2'];
        // set session
        $param = new Zend_Session_Namespace('par_so');
        $param->id_customer=$id_customer;
        $param->closed=$closed;
        $param->tgl1=$tgl1;
        $param->tgl2=$tgl2;
    }

    function listAction(){
        // Title Browser
        $this->view->title = "Daftar SO";
        // Navbar
        $this->_helper->navbar('so',0,'so/new',0,0);
        // collapse
        $this->view->sidecolaps="sidebar-collapse";
        // get param
        $param = new Zend_Session_Namespace('par_so');
        $id_customer=$param->id_customer;
        $closed=$param->closed;
        $tgl1=$param->tgl1;
        $tgl2=$param->tgl2;
        // get data
        $so=new So();
        $getSo=$so->querySo($id_customer,$closed, $tgl1, $tgl2);
        $this->view->listSo=$getSo;
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data So";
        // navigation
        $this->_helper->navbar(0,'so',0,0,0);
        // so
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $tgl_so = $request['tgl_so'];
        $keterangan = $this->_helper->string->esc_quote(trim($request['keterangan']));
        $booking = $request['booking'];
        $uang_muka = $request['uang_muka'];
        $id_customer = $this->_helper->string->esc_quote(trim($request['id_customer']));
        $id_rumah = $this->_helper->string->esc_quote(trim($request['id_rumah']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        
        if($vd->validasiLength($tgl_so,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Tanggal so tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($keterangan,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Keterangan tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiBetween($booking,1,999999999999999)=='F'){
            $err++;
            $msg=$msg."<strong>- Booking harus lebih dari 0</strong><br>";
        }
        if($vd->validasiBetween($uang_muka,1,999999999999999)=='F'){
            $err++;
            $msg=$msg."<strong>- Uang Muka harus lebih dari 0</strong><br>";
        }
        if($vd->validasiLength($id_customer,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- ID Customer tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_rumah,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- ID Rumah tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $so = new So();
           $setSo = $so->setSo($tgl_so,$keterangan,$booking,$uang_muka,$id_customer,$id_rumah);
            echo $setSo;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $so = new So();
        $delSo=$so->delSo($id);
        echo $delSo;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data So";
        // Navbar
        $this->_helper->navbar('so',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $so=new So();
        $getSo=$so->getSoById($id);
        if(!$getSo){
            $this->view->eksis="f";
        }else{
            foreach ($getSo as $data){
                $this->view->id_so=$data['id_so'];
                $no_so=$data['no_so'];
                $this->view->tgl_so=$data['tgl_so_fmt'];
                $this->view->keterangan=$data['keterangan'];
                $this->view->booking=$data['booking_v'];
                $this->view->uang_muka=$data['uang_muka'];
                $this->view->id_customer=$data['id_customer'];
                $this->view->nm_customer=$data['nm_customer'];
                $this->view->kontak=$data['kontak'];
                $this->view->id_rumah=$data['id_rumah'];
                $this->view->nm_rumah=$data['nm_rumah'];
            }
            $this->view->title = "Edit Data So ".$no_so;
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id= $request['id'];
        $tgl_so = $request['tgl_so'];
        $keterangan = $this->_helper->string->esc_quote(trim($request['keterangan']));
        $booking = $request['booking'];
        $uang_muka = $request['uang_muka'];
        $id_customer = $this->_helper->string->esc_quote(trim($request['id_customer']));
        $id_rumah = $this->_helper->string->esc_quote(trim($request['id_rumah']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        
        if($vd->validasiLength($tgl_so,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Tanggal so tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($keterangan,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Keterangan tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiBetween($booking,1,999999999999999)=='F'){
            $err++;
            $msg=$msg."<strong>- Booking harus lebih dari 0</strong><br>";
        }
        if($vd->validasiBetween($uang_muka,1,999999999999999)=='F'){
            $err++;
            $msg=$msg."<strong>- Uang Muka harus lebih dari 0</strong><br>";
        }
        if($vd->validasiLength($id_customer,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- ID Customer tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_rumah,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- ID Rumah tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $so = new So();
            $updSo = $so->updSo($tgl_so,$keterangan,$booking,$uang_muka,$id_customer,$id_rumah,$id);
            echo $updSo;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}