<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class OincomeController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Oincome');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst="active";
        $this->view->active_oincome="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Oincome";
        // navigation
        $this->_helper->navbar(0,0,'oincome/new',0,0);
        $oincome= new oincome();
        $this->view->listOincome=$oincome->fetchAll();
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Oincome";
        // navigation
        $this->_helper->navbar(0,'oincome',0,0,0);
        // oincome
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $nm_oincome = $this->_helper->string->esc_quote(trim($request['nm_oincome']));
        $id_branch = $this->_helper->string->esc_quote(trim($request['id_branch']));
        $aktif= "t";
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_oincome,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama Oincome tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_branch,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- ID Branch tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $oincome = new Oincome();
           $setOincome = $oincome->setOincome($nm_oincome);
            echo $setOincome;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $oincome = new oincome();
        $deloincome=$oincome->deloincome($id);
        echo $deloincome;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Oincome";
        // Navbar
        $this->_helper->navbar('oincome',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $oincome=new oincome();
        $getoincome=$oincome->getoincomeById($id);
        if(!$getoincome){
            $this->view->eksis="f";
        }else{
            foreach ($getoincome as $data){
                $this->view->id=$data['id_oincome'];
                $this->view->nm=$data['nm_oincome'];
            }
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_oincome= $request['id'];
        $nm_oincome = $this->_helper->string->esc_quote(trim($request['nm_oincome']));
        $id_branch = $this->_helper->string->esc_quote(trim($request['id_branch']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_oincome,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama oincome tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_branch,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- ID Branch tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $oincome = new oincome();
            $updoincome = $oincome->updoincome($nm_oincome,$id_oincome);
            echo $updoincome;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}
