<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class DebiturController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Debitur');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst_kontak="active";
        $this->view->active_debitur="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Debitur";
        // navigation
        $this->_helper->navbar(0,0,'debitur/new',0,0);
        $debitur= new Debitur();
        $this->view->listDebitur=$debitur->fetchAll();

    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Debitur";
        // navigation
        $this->_helper->navbar(0,'debitur',0,0,0);
        // debitur
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_kontak = $this->_helper->string->esc_quote(trim($request['id_kontak']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($id_kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama debitur tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
       
        if($err==0){
            $debitur = new Debitur();
           $setdebitur = $debitur->setDebitur($id_kontak);
            echo $setdebitur;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $debitur = new Debitur();
        $deldebitur=$debitur->delDebitur($id);
        echo $deldebitur;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Debitur";
        // Navbar
        $this->_helper->navbar('debitur',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $debitur=new Debitur();
        $getdebitur=$debitur->getDebiturById($id);
        if(!$getdebitur){
            $this->view->eksis="f";
        }else{
            foreach ($getdebitur as $data){
                $this->view->id=$data['id_debitur'];
                $this->view->nm_debit_tur=$data['nm_debit_tur'];
                $this->view->id_kontak=$data['id_kontak'];
            }
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_debitur= $request['id'];
        $id_kontak = $this->_helper->string->esc_quote(trim($request['id_kontak']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($id_kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama debitur tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $debitur = new Debitur();
            $upddebitur = $debitur->updDebitur($id_kontak,$id_debitur);
            echo $upddebitur;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}