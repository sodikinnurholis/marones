<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class SaKasController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('SaKas');
        Zend_Loader::loadClass('Kas');
        Zend_Loader::loadClass('Branch');
        Zend_Loader::loadClass('TipeKas');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mts="active";
        $this->view->active_sakas="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Saldo Awal Kas";
        // navigation
        $this->_helper->navbar(0,0,'sakas/new',0,0);
        Zend_Session::namespaceUnset('par_sakas');
        $kas=new Kas();
        $this->view->listKas=$kas->fetchAll();
        $branch=new Branch();
        $this->view->listBranch=$branch->fetchAll();
        $tipekas=new TipeKas();
        $this->view->listTipeKas=$tipekas->fetchAll();
    }

    function ashowAction(){
        // makes disable layout
        $this->_helper->getHelper('layout')->disableLayout();
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $id_kas=$request['id_kas'];
        $id_branch=$request['id_branch'];
        $id_tipe_kas=$request['id_tipe_kas'];
        $tgl1=$request['tgl1'];
        $tgl2=$request['tgl2'];
        // set session
        $param = new Zend_Session_Namespace('par_sakas');
        $param->id_kas=$id_kas;
        $param->id_branch=$id_branch;
        $param->id_tipe_kas=$id_tipe_kas;
        $param->tgl1=$tgl1;
        $param->tgl2=$tgl2;
    }
    
    function listAction(){
        // Title Browser
        $this->view->title = "Daftar Saldo Awal Kas";
        // Navbar
        $this->_helper->navbar('sakas',0,'sakas/new',0,0);
        // collapse
        $this->view->sidecolaps="sidebar-collapse";
        // get param
        $param = new Zend_Session_Namespace('par_sakas');
        $id_kas=$param->id_kas;
        $id_branch=$param->id_branch;
        $id_tipe_kas=$param->id_tipe_kas;
        $tgl1=$param->tgl1;
        $tgl2=$param->tgl2;
        // get data
        $sakas=new SaKas();
        $getSaKas=$sakas->querySaKas($id_kas,$id_branch,$id_tipe_kas, $tgl1, $tgl2);
        $this->view->listSaKas=$getSaKas;
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Saldo Awal Kas";
        // navigation
        $this->_helper->navbar(0,'sakas',0,0,0);
        // ri
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_kas = $this->_helper->string->esc_quote(trim($request['id_kas']));
        $tgl_saldo_awal = $this->_helper->string->esc_quote(trim($request['tgl_saldo_awal']));
        $nominal_sa = $request['nominal_sa'];
       
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        
        if($vd->validasiLength($id_kas,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Kas tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($nominal_sa,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nominal Saldo tidak boleh kosong maksimal 50 angka</strong><br>";
        }
        if($vd->validasiLength($tgl_saldo_awal,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Tanggal Saldo tidak boleh kosong</strong><br>";
        }
       
         if($err==0){
            $sakas = new SaKas();
           $setSaKas = $sakas->setSaKas($id_kas,$nominal_sa,$tgl_saldo_awal);
            echo $setSaKas;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $sakas = new SaKas();
        $delSaKas=$sakas->delSaKas($id);
        echo $delSaKas;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Saldo Awal Kas";
        // Navbar
        $this->_helper->navbar('sakas',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $sakas=new SaKas();
        $getSaKas=$sakas->getSaKasById($id);
        if(!$getSaKas){
            $this->view->eksis="f";
        }else{
            foreach ($getSaKas as $data){
                $this->view->id_kas=$data['id_kas'];
                $this->view->nm_kas=$data['nm_kas'];
                $this->view->nm_branch=$data['nm_branch'];
                $this->view->nominal_sa=$data['nominal_sa'];
                $this->view->tgl=$data['tgl_saldo_awal_fmt'];
            }
            $this->view->title = "Edit Data Saldo Awal Kas ";
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_kas = $request['id_kas'];
        $tgl_saldo_awal = $this->_helper->string->esc_quote(trim($request['tgl_saldo_awal']));
        $nominal_sa = $request['nominal_sa'];
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        
        
        if($vd->validasiLength($nominal_sa,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nominal Saldo tidak boleh kosong maksimal 50 angka</strong><br>";
        }
        if($vd->validasiLength($tgl_saldo_awal,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Tanggal Saldo tidak boleh kosong</strong><br>";
        }
         if($err==0){
            $sakas = new SaKas();
           $setSaKas = $sakas->updSaKas($nominal_sa,$tgl_saldo_awal,$id_kas);
            echo $setSaKas;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}