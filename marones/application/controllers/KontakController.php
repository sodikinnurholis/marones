<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class KontakController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Kontak');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst_kontak="active";
        $this->view->active_kontak="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Kontak";
        // navigation
        $this->_helper->navbar(0,0,'kontak/new',0,0);
        $kontak= new Kontak();
        $this->view->listKontak=$kontak->fetchAll();
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Kontak";
        // navigation
        $this->_helper->navbar(0,'kontak',0,0,0);
        // kontak
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request        = $this->getRequest()->getPost();
        $nm_kontak      = $this->_helper->string->esc_quote(trim($request['nm_kontak']));
        $id_type_kontak = $this->_helper->string->esc_quote(trim($request['id_type_kontak']));
        $tlp            = $request['tlp'];
        $email          = $this->_helper->string->esc_quote(trim($request['email']));
        $alamat         = $this->_helper->string->esc_quote(trim($request['alamat']));
        $aktif= "t";
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama Kontak tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_type_kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Tipe Kontak tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($tlp,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Telepon tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($email,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Email tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($alamat,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Alamat tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $kontak = new Kontak();
           $setKontak = $kontak->setKontak($nm_kontak,$id_type_kontak,$tlp,$email,$alamat);
            echo $setKontak;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $kontak = new Kontak();
        $delKontak=$kontak->delKontak($id);
        echo $delKontak;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Kontak";
        // Navbar
        $this->_helper->navbar('kontak',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $kontak=new Kontak();
        $getKontak=$kontak->getKontakById($id);
        if(!$getKontak){
            $this->view->eksis="f";
        }else{
            foreach ($getKontak as $data){
                $this->view->id_kontak=$data['id_kontak'];
                $this->view->nm_kontak=$data['nm_kontak'];
                $this->view->id_type_kontak=$data['id_type_kontak'];
                $this->view->nm_type_kontak=$data['nm_type_kontak'];
                $this->view->tlp=$data['tlp'];
                $this->view->email=$data['email'];
                $this->view->alamat=$data['alamat'];
            }
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_kontak= $request['id_kontak'];
        $nm_kontak = $this->_helper->string->esc_quote(trim($request['nm_kontak']));
        $id_type_kontak = $this->_helper->string->esc_quote(trim($request['id_type_kontak']));
        $tlp          = $request['tlp'];
        $email = $this->_helper->string->esc_quote(trim($request['email']));
        $alamat = $this->_helper->string->esc_quote(trim($request['alamat']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama Kontak tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_type_kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- ID Tipe Kontak tidak boleh kosong maksimal 50 huruf</strong><br>";
        }        
        if($vd->validasiLength($tlp,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Telepon tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($email,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Email tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($alamat,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Alamat tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $kontak = new Kontak();
            $updKontak = $kontak->updKontak($nm_kontak,$id_type_kontak,$tlp,$email,$alamat,$id_kontak);
            echo $updKontak;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}
