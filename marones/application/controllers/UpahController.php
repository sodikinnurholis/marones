<?php
/*
	Programmer	: Tiar Aristian
	Release		: Januari 2016
	Module		: Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class UpahController extends MasterController
{
	function init()
	{
	    parent::init();
		$this->initView();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('Upah');
		Zend_Loader::loadClass('Zend_Session');
		Zend_Loader::loadClass('Zend_Layout');
		// layout
		$this->_helper->layout()->setLayout('main');
		// treeview
		$this->view->active_mst="active";
		$this->view->active_upah="active";
		
	}

	function indexAction()
	{
		// Title Browser
		$this->view->title = "Upah";
		// navigation
		$this->_helper->navbar(0,0,'upah/new',0,0);
		$upah= new Upah();
		$this->view->listUpah=$upah->fetchAll();
	}
	
	function newAction()
	{
	  
	    // Title Browser
	    $this->view->title = "Input Data Upah";
        // navigation
        $this->_helper->navbar(0,'upah',0,0,0);
	    // upah
	   
	}
	
	function ainsAction(){
	    // disable layout
	    $this->_helper->layout->disableLayout();
	    // start inserting
	    $request = $this->getRequest()->getPost();
	    $nm_upah = $this->_helper->string->esc_quote(trim($request['nm']));
	    $aktif= "t";
	    // validation
	    $err=0;
	    $msg="";
	    $vd = new Validation();
	    if($vd->validasiLength($nm_upah,1,50)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama upah tidak boleh kosong maksimal 50 huruf</strong><br>";
	    }
	    if($err==0){
	        $upah = new Upah();
	       $setUpah = $upah->setUpah($nm_upah);
	        echo $setUpah;
	    }else{
	        echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
	    }
	}
	
	function adelAction(){
	    // disabel layout
	    $this->_helper->layout->disableLayout();
	    // set database
	    $request = $this->getRequest()->getPost();
	    // gets value from ajax request
	    $param = $request['param'];
	    $id = $param[0];
	    $upah = new Upah();
	    $delUpah=$upah->delUpah($id);
	    echo $delUpah;
	}
	
	function editAction()
	{
	    // Title Browser
	    $this->view->title = "Edit Data Upah";
	    // Navbar
	    $this->_helper->navbar('Upah',0,0,0,0);
	    // get data
	    $id=$this->_request->get('id');
	    $upah=new Upah();
	    $getUpah=$upah->getUpahById($id);
	    if(!$getUpah){
	        $this->view->eksis="f";
	    }else{
	        foreach ($getUpah as $data){
	            $this->view->id=$data['id_upah'];
	            $this->view->nm=$data['nm_upah'];
	        }
	    }
	    
	}
	
	function aupdAction(){
	    // disable layout
	    $this->_helper->layout->disableLayout();
	    // start inserting
	    $request = $this->getRequest()->getPost();
	    $id_upah= $request['id'];
	    $nm_upah = $this->_helper->string->esc_quote(trim($request['nm']));
	    // validation
	    $err=0;
	    $msg="";
	    $vd = new Validation();
	    if($vd->validasiLength($nm_upah,1,50)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama upah tidak boleh kosong maksimal 50 huruf</strong><br>";
	    }
	    if($err==0){
	        $upah = new Upah();
	        $updUpah = $upah->updUpah($nm_upah,$id_upah);
	        echo $updUpah;
	    }else{
	        echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
	    }
	}
	
}