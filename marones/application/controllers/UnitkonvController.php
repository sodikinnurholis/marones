<?php
/*
	Programmer	: Tiar Aristian
	Release		: Januari 2016
	Module		: Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class UnitkonvController extends MasterController
{
	function init()
	{
	    parent::init();
		$this->initView();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('Unitkonv');
		Zend_Loader::loadClass('Zend_Session');
		Zend_Loader::loadClass('Zend_Layout');
		// layout
		$this->_helper->layout()->setLayout('main');
		// treeview
		$this->view->active_mst="active";
		$this->view->active_unitkonv="active";
		
	}

	function indexAction()
	{
		// Title Browser
		$this->view->title = "Unit konversi";
		// navigation
		$this->_helper->navbar(0,0,'unitkonv/new',0,0);
		$unitkonv= new Unitkonv();
		$this->view->listUnitkonv=$unitkonv->fetchAll();
	}
	
	function newAction()
	{
	  
	    // Title Browser
	    $this->view->title = "Input Data Unit Konversi";
        // navigation
        $this->_helper->navbar(0,'unitkonv',0,0,0);
	    // unitkonv kerja
	   
	}
	
	function ainsAction(){
	    // disable layout
	    $this->_helper->layout->disableLayout();
	    // start inserting
	    $request = $this->getRequest()->getPost();
	    $u_from = $this->_helper->string->esc_quote(trim($request['id_unit']));
		$u_to = $this->_helper->string->esc_quote(trim($request['id_unit2']));
		$konv=$request['konv'];
	    // validation
	    $err=0;
	    $msg="";
	    $vd = new Validation();
	    if($vd->validasiLength($u_from,1,100)=='F'){
			$err++;
			$msg=$msg."<strong>- Unit asal tidak boleh kosong</strong><br>";
		}
		if($vd->validasiLength($u_to,1,100)=='F'){
			$err++;
			$msg=$msg."<strong>- Unit konversi tidak boleh kosong</strong><br>";
		}
		if(($u_from==$u_to)and(($u_from!='') or ($u_to!=''))){
			$err++;
			$msg=$msg."<strong>- Unit asal dan unit konversi tidak boleh sama</strong><br>";
		}
	    if($err==0){
	        $unitkonv = new Unitkonv();
	       $setUnitkonv = $unitkonv->setUnitkonv($u_from,$u_to,$konv);
	        echo $setUnitkonv;
	    }else{
	        echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
	    }
	}
	
	function adelAction(){
	    // disabel layout
	    $this->_helper->layout->disableLayout();
	    // set database
	    $request = $this->getRequest()->getPost();
	    // gets value from ajax request
	    $param = $request['param'];
	    $id1 = $param[0];
	    $id2 = $param[1];
	    $unit = new Unitkonv();
	    $delUnitkonv=$unit->delUnitkonv($id1,$id2);
	    echo $delUnitkonv;
	}
	
	function editAction()
	{
	    // Title Browser
	    $this->view->title = "Edit Data Unit Konversi";
	    // Navbar
	    $this->_helper->navbar('unitkonv',0,0,0,0);
	    // get data
	    $id1=$this->_request->get('id1');
    	$id2=$this->_request->get('id2');
    	$unitKonv=new Unitkonv();
    	$getUnitkonv=$unitKonv->getUnitkonvById($id1,$id2);
	    if(!$getUnitkonv){
	        $this->view->eksis="f";
	    }else{
	        foreach ($getUnitkonv as $data){
	            $this->view->id1=$data['id_unit_from'];
    			$this->view->id2=$data['id_unit_to'];
    			$this->view->nm1=$data['nm_unit_from'];
    			$this->view->nm2=$data['nm_unit_to'];
    			$this->view->konv=$data['konv'];
	        }
	    }
	    
	}
	
	function aupdAction(){
	    // disable layout
	    $this->_helper->layout->disableLayout();
	    // start inserting
	    $request = $this->getRequest()->getPost();
	    $u_from = $this->_helper->string->esc_quote(trim($request['u1']));
		$u_to = $this->_helper->string->esc_quote(trim($request['u2']));
		$konv=$request['konv'];
	    // validation
	    $err=0;
	    $msg="";
	    $vd = new Validation();
	    if($vd->validasiLength($u_from,1,100)=='F'){
			$err++;
			$msg=$msg."<strong>- Unit asal tidak boleh kosong</strong><br>";
		}
		if($vd->validasiLength($u_to,1,100)=='F'){
			$err++;
			$msg=$msg."<strong>- Unit konversi tidak boleh kosong</strong><br>";
		}
		if(($u_from==$u_to)and(($u_from!='') or ($u_to!=''))){
			$err++;
			$msg=$msg."<strong>- Unit asal dan unit konversi tidak boleh sama</strong><br>";
		}
	    if($err==0){
	        $unitkonv = new Unitkonv();
	        $updUnitkonv = $unitkonv->updUnitkonv($u_from,$u_to,$konv);
	        echo $updUnitkonv;
	    }else{
	        echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
	    }
	}
	
}