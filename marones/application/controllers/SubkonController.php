<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class SubkonController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Subkon');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst="active";
        $this->view->active_subkon="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Subkon";
        // navigation
        $this->_helper->navbar(0,0,'subkon/new',0,0);
        $subkon= new Subkon();
        $this->view->listSubkon=$subkon->fetchAll();
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Subkon";
        // navigation
        $this->_helper->navbar(0,'subkon',0,0,0);
        // subkon
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $nm_subkon = $this->_helper->string->esc_quote(trim($request['nm']));
        $alamat = $this->_helper->string->esc_quote(trim($request['alamat']));
        $kontak = $this->_helper->string->esc_quote(trim($request['kontak']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_subkon,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama subkon tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($alamat,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Alamat tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Kontak tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $subkon = new Subkon();
           $setSubkon = $subkon->setSubkon($nm_subkon,$alamat,$kontak);
            echo $setSubkon;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $subkon = new Subkon();
        $delSubkon=$subkon->delSubkon($id);
        echo $delSubkon;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Subkon";
        // Navbar
        $this->_helper->navbar('Subkon',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $subkon=new Subkon();
        $getSubkon=$subkon->getSubkonById($id);
        if(!$getSubkon){
            $this->view->eksis="f";
        }else{
            foreach ($getSubkon as $data){
                $this->view->id=$data['id_subkon'];
                $this->view->nm=$data['nm_subkon'];
                $this->view->alamat=$data['alamat'];
                $this->view->kontak=$data['kontak'];
            }
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id= $request['id'];
        $nm_subkon = $this->_helper->string->esc_quote(trim($request['nm']));
        $alamat = $this->_helper->string->esc_quote(trim($request['alamat']));
        $kontak = $this->_helper->string->esc_quote(trim($request['kontak']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_subkon,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama subkon tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($alamat,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Alamat tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($kontak,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Kontak tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $subkon = new Subkon();
            $updSubkon = $subkon->updSubkon($nm_subkon,$alamat,$kontak,$id);
            echo $updSubkon;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}