<?php
/*
    Programmer  : Tiar Aristian
    Release     : Januari 2016
    Module      : Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class RiController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Ri');
        Zend_Loader::loadClass('Supplier');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_trs="active";
        $this->view->active_ri="active";
        
    }

    function indexAction()
    {
        // Title Browser
        $this->view->title = "Ri";
        // navigation
        $this->_helper->navbar(0,0,'ri/new',0,0);
        Zend_Session::namespaceUnset('par_ri');
        $supplier=new Supplier();
        $this->view->listSupplier=$supplier->fetchAll();
    }

    function ashowAction(){
        // makes disable layout
        $this->_helper->getHelper('layout')->disableLayout();
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $id_supplier=$request['id_supplier'];
        $closed=$request['status'];
        $tgl1=$request['tgl1'];
        $tgl2=$request['tgl2'];
        // set session
        $param = new Zend_Session_Namespace('par_ri');
        $param->id_supplier=$id_supplier;
        $param->closed=$closed;
        $param->tgl1=$tgl1;
        $param->tgl2=$tgl2;
    }
    
    function listAction(){
        // Title Browser
        $this->view->title = "Daftar Ri";
        // Navbar
        $this->_helper->navbar('ri',0,'ri/new',0,0);
        // collapse
        $this->view->sidecolaps="sidebar-collapse";
        // get param
        $param = new Zend_Session_Namespace('par_ri');
        $id_supplier=$param->id_supplier;
        $closed=$param->closed;
        $tgl1=$param->tgl1;
        $tgl2=$param->tgl2;
        // get data
        $ri=new Ri();
        $getRi=$ri->queryRi($id_supplier,$closed, $tgl1, $tgl2);
        $this->view->listRi=$getRi;
    }
    
    function newAction()
    {
      
        // Title Browser
        $this->view->title = "Input Data Ri";
        // navigation
        $this->_helper->navbar(0,'ri',0,0,0);
        // ri
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_po = $this->_helper->string->esc_quote(trim($request['id_po']));
        $tgl_ri = $this->_helper->string->esc_quote(trim($request['tgl_ri']));
        $id_supplier = $this->_helper->string->esc_quote(trim($request['id_supplier']));
        $vat_in = $request['vat_in'];
        $diskon_ri = $request['diskon_ri'];
        $no_inv_supp = $this->_helper->string->esc_quote(trim($request['no_inv_supp']));
        $status =0;
        $closed ="f";
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        
        if($vd->validasiLength($id_po,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- PO tidak boleh koring maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($tgl_ri,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Tanggal ri tidak boleh koring maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_supplier,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Keterangan tidak boleh koring maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiBetween($vat_in,0,10)=='F'){
            $err++;
            $msg=$msg."<strong>- Vat In tidak boleh kosong</strong><br>";
        }
        if($vd->validasiBetween($diskon_ri,1,999999999999999)=='F'){
            $err++;
            $msg=$msg."<strong>- diskon harus lebih dari 0</strong><br>";
        }
        if($vd->validasiLength($no_inv_supp,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- No Invoice Supplier tidak boleh koring maksimal 50 huruf</strong><br>";
        }
         if($err==0){
            $ri = new Ri();
           $setRi = $ri->setRi($id_po,$tgl_ri,$id_supplier,$vat_in,$diskon_ri,$no_inv_supp,$status,$closed);
            echo $setRi;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $ri = new Ri();
        $delRi=$ri->delRi($id);
        echo $delRi;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Ri";
        // Navbar
        $this->_helper->navbar('ri',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $ri=new Ri();
        $getRi=$ri->getRiById($id);
        if(!$getRi){
            $this->view->eksis="f";
        }else{
            foreach ($getRi as $data){
                $this->view->id_ri=$data['id_ri'];
                $no_ri=$data['no_ri'];
                $this->view->id_po=$data['id_po'];
                $this->view->no_po=$data['no_po'];
                $this->view->tgl_po=$data['tgl_pori_fmt'];
                $this->view->id_supplier=$data['id_supplier'];
                $this->view->nm_supplier=$data['nm_supplier'];
                $this->view->keterangan=$data['keterangan'];
                $this->view->tgl_ri=$data['tgl_ri_fmt'];
                $this->view->vat_in=$data['vat_in'];
                $this->view->diskon_ri=$data['diskon_ri'];
                $this->view->no_inv_supp=$data['no_inv_supp'];
                $status=$data['status'];
                $closed=$data['closed'];      
            }
            $this->view->title = "Edit Data Ri ".$no_ri;
        }
        
    }
    
    function aupdAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id= $request['id'];
        $id_po = $this->_helper->string->esc_quote(trim($request['id_po']));
        $tgl_ri = $this->_helper->string->esc_quote(trim($request['tgl_ri']));
        $id_supplier = $this->_helper->string->esc_quote(trim($request['id_supplier']));
        $vat_in = $request['vat_in'];
        $diskon_ri = $request['diskon_ri'];
        $no_inv_supp = $this->_helper->string->esc_quote(trim($request['no_inv_supp']));
        $status =0;
        $closed ="f";
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        
        if($vd->validasiLength($id_po,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- PO tidak boleh koring maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($tgl_ri,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Tanggal ri tidak boleh koring maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($id_supplier,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Keterangan tidak boleh koring maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiBetween($vat_in,0,10)=='F'){
            $err++;
            $msg=$msg."<strong>- Vat In tidak boleh kosong</strong><br>";
        }
        if($vd->validasiBetween($diskon_ri,1,999999999999999)=='F'){
            $err++;
            $msg=$msg."<strong>- diskon harus lebih dari 0</strong><br>";
        }
        if($vd->validasiLength($no_inv_supp,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- No Invoice Supplier tidak boleh koring maksimal 50 huruf</strong><br>";
        }
         if($err==0){
            $ri = new Ri();
           $setRi = $ri->updRi($id_po,$tgl_ri,$id_supplier,$vat_in,$diskon_ri,$no_inv_supp,$status,$closed,$id);
            echo $setRi;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}