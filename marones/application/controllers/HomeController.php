<?php
/*
	Programmer	: Tiar Aristian
	Release		: Januari 2016
	Module		: Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class HomeController extends MasterController
{
	function init()
	{
	    parent::init();
		$this->initView();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		// layout
		$this->_helper->layout()->setLayout('main');
	}

	function indexAction()
	{
		// Title Browser
		$this->view->title = "Beranda Marones";
		// navigation
		$this->_helper->navbar(0,0,0,0,0);
		
	}

	function aksesAction()
	{
		// Title Browser
		$this->view->title = "Halaman Alih Marones";
		$this->view->desc = "Tidak ada akses untuk user";
		// navigation
		$this->_helper->navbar('home',0,0,0,0);
	}
}