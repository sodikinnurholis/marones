<?php
/*
 Programmer	: Tiar Aristian
 Release		: Januari 2016
 Module		: Home Controller -> Controller untuk modul home
 */
include 'MasterController.php';

class PangkatController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('Pangkat');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        $auth = Zend_Auth::getInstance();
        $ses_hrd = new Zend_Session_Namespace('ses_hrd');
        if (($auth->hasIdentity())and($ses_hrd->uname)) {
            $this->view->namauser =Zend_Auth::getInstance()->getIdentity()->nm_sdm;
            $this->view->username=Zend_Auth::getInstance()->getIdentity()->username;
        }else{
            $this->_redirect('/');
        }
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst="active";
        $this->view->active_pangkat="active";
        
    }
    
    function indexAction()
    {
        // Title Browser
        $this->view->title = "Pangkat";
        // navigation
        $this->_helper->navbar(0,0,'pangkat/new',0,0);
        $pangkat= new Pangkat();
        $this->view->listPangkat=$pangkat->fetchAll();
    }
    
    function newAction()
    {
        
        // Title Browser
        $this->view->title = "Input Data Pangkat";
        // navigation
        $this->_helper->navbar(0,'pangkat',0,0,0);
       
    }
    
    function ainsAction(){
        // disable layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $nm_pangkat = $this->_helper->string->esc_quote(trim($request['nm']));
        $golongan = $this->_helper->string->esc_quote(trim($request['gol']));
        $skor =intval($request['skor']);
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_pangkat,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama pangkat tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($golongan,1,10)=='F'){
            $err++;
            $msg=$msg."<strong>- Golongan tidak boleh kosong maksimal 10 huruf</strong><br>";
        }
        if($vd->validasiBetween($skor,1,20)=='F'){
            $err++;
            $msg=$msg."<strong>- Skor antara 1 - 20 </strong><br>";
        }
        if($err==0){
            $pangkat = new Pangkat();
            $setPangkat = $pangkat->setPangkat($nm_pangkat, $golongan, $skor);
            echo $setPangkat;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction(){
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $pangkat = new Pangkat();
        $delPangkat=$pangkat->delPangkat($id);
        echo $delPangkat;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Pangkat";
        // Navbar
        $this->_helper->navbar('pangkat',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $pangkat=new Pangkat();
        $getPangkat=$pangkat->getPangkatById($id);
        if(!$getPangkat){
            $this->view->eksis="f";
        }else{
            foreach ($getPangkat as $data){
                $this->view->id=$data['id_pangkat'];
                $this->view->nm=$data['nm_pangkat'];
                $this->view->gol=$data['golongan'];
                $this->view->skor=$data['skor'];
            }
        }
    }
    
    function aupdAction()
    {
        // disabel layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_pangkat = $request['id'];
        $nm_pangkat = $this->_helper->string->esc_quote(trim($request['nm']));
        $golongan = $this->_helper->string->esc_quote(trim($request['gol']));
        $skor =intval($request['skor']);
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($nm_pangkat,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Nama pangkat tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($vd->validasiLength($golongan,1,10)=='F'){
            $err++;
            $msg=$msg."<strong>- Golongan tidak boleh kosong maksimal 10 huruf</strong><br>";
        }
        if($vd->validasiBetween($skor,1,20)=='F'){
            $err++;
            $msg=$msg."<strong>- Skor antara 1 - 20 </strong><br>";
        }
        if($err==0){
            $pangkat = new Pangkat();
            $updPangkat = $pangkat->updPangkat($nm_pangkat, $golongan, $skor, $id_pangkat);
            echo $updPangkat;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
       
    }
    
    
}