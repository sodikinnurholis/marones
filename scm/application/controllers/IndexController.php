<?php
/*
    Vendor      : Cebersoft
    Release     : Oktober 2016
    Module      : Index Controller -> Controller untuk Login
*/

include 'MasterController.php';

class IndexController extends MasterController
{
    function init()
    {
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        $this->view->publicUrl = Zend_Registry::get('PUBLIC_URL');
        Zend_Loader::loadClass('Sdm');
        Zend_Loader::loadClass('Validation');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');

        $auth = Zend_Auth::getInstance();
        $ses_hrd = new Zend_Session_Namespace('ses_hrd');
        if (($auth->hasIdentity())and($ses_hrd->uname)) {
            $this->_redirect('/home');
        }
    }

    function indexAction()
    {   
        // Title Browser
        $this->view->title = "Login Manajemen SIMP";
        // disabel layout
        $this->_helper->layout->disableLayout();
        
        // message
        $e_msg = new Zend_Session_Namespace('e_msg');
        $this->view->message = $e_msg->msg_dsc;
        if ($this->_request->isPost()) {
            // collect the data from the user
            Zend_Loader::loadClass('Zend_Filter_StripTags');
            $f = new Zend_Filter_StripTags();
            $username = $f->filter($this->_request->getPost('txtUsername'));
            $password = $f->filter($this->_request->getPost('txtPass'));
            $password = md5($password);
            if (empty($username)) {
                $e_msg = new Zend_Session_Namespace('e_msg');
                $e_msg->msg_dsc = "Username tidak boleh kosong!";
                $e_msg->setExpirationSeconds(5);
                $this->_redirect('/');
            } else {
                // setup Zend_Auth adapter for a database table
                Zend_Loader::loadClass('Zend_Auth_Adapter_DbTable');
                $dbAdapter = Zend_Registry::get('dbAdapter');
                $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
                $authAdapter->setTableName('sdm.v_sdm');
                $authAdapter->setIdentityColumn('username');
                $authAdapter->setCredentialColumn('pwd');
                // Set the input credential values to authenticate against
                $authAdapter->setIdentity($username);
                $authAdapter->setCredential($password);
                // do the authentication
                $auth = Zend_Auth::getInstance();
                $resultAc = $auth->authenticate($authAdapter);
                if ($resultAc->isValid()) {
                    $data = $authAdapter->getResultRowObject(null,'password');
                    $auth->getStorage()->write($data);
                    //session
                    $ses_hrd = new Zend_Session_Namespace('ses_hrd');
                    $ses_hrd->uname = $username;
                    $ses_hrd->setExpirationSeconds(7200);
                    // get sdm
                    $Sdm = new Sdm();
                    $getSdm=$Sdm->getSdmByUsername($username);
                    $id_sdm="";
                    foreach ($getSdm as $dtSdm){
                    	$id_sdm=$dtSdm['id_sdm'];
                    }
                    $ses_hrd->id = $id_sdm;
                    $arr_jabatan=array();
                    $superadm="f";
                    $this->_redirect('home');
                }else{
                    $e_msg = new Zend_Session_Namespace('e_msg');
                    $e_msg->msg_dsc = "Login gagal!<br>Username atau Password salah";
                    $e_msg->setExpirationSeconds(5);
                    $this->_redirect('/');
                }
            }
        }
    }
}