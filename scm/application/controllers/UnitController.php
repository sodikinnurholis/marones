<?php
/*
	Programmer	: Tiar Aristian
	Release		: Januari 2016
	Module		: Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class UnitController extends MasterController
{
	function init()
	{
	    parent::init();
		$this->initView();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('UnitKerja');
		Zend_Loader::loadClass('Jabstruk');
		Zend_Loader::loadClass('Jabfung');
		Zend_Loader::loadClass('Zend_Session');
		Zend_Loader::loadClass('Zend_Layout');
		$auth = Zend_Auth::getInstance();
		$ses_hrd = new Zend_Session_Namespace('ses_hrd');
		if (($auth->hasIdentity())and($ses_hrd->uname)) {
			$this->view->namauser =Zend_Auth::getInstance()->getIdentity()->nm_sdm;
			$this->view->username=Zend_Auth::getInstance()->getIdentity()->username;
		}else{
			$this->_redirect('/');
		}
		// layout
		$this->_helper->layout()->setLayout('main');
		// treeview
		$this->view->active_mst="active";
		$this->view->active_unit="active";
		
	}

	function indexAction()
	{
		// Title Browser
		$this->view->title = "Unit Kerja";
		// navigation
		$this->_helper->navbar(0,0,'unit/new',0,0);
		$unit= new UnitKerja();
		$this->view->listUnit=$unit->fetchAll();
	}
	
	function newAction()
	{
	  
	    // Title Browser
	    $this->view->title = "Input Data Unit Kerja";
        // navigation
        $this->_helper->navbar(0,'unit',0,0,0);
	    // unit kerja
	   
	}
	
	function ainsAction(){
	    // disable layout
	    $this->_helper->layout->disableLayout();
	    // start inserting
	    $request = $this->getRequest()->getPost();
	    $nm_unit = $this->_helper->string->esc_quote(trim($request['nm']));
	    $aktif= "t";
	    // validation
	    $err=0;
	    $msg="";
	    $vd = new Validation();
	    if($vd->validasiLength($nm_unit,1,50)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama unit tidak boleh kosong maksimal 50 huruf</strong><br>";
	    }
	    if($err==0){
	        $unit = new UnitKerja();
	       $setUnit = $unit->setUnitKerja($nm_unit, $aktif);
	        echo $setUnit;
	    }else{
	        echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
	    }
	}
	
	function adelAction(){
	    // disabel layout
	    $this->_helper->layout->disableLayout();
	    // set database
	    $request = $this->getRequest()->getPost();
	    // gets value from ajax request
	    $param = $request['param'];
	    $id = $param[0];
	    $unit = new UnitKerja();
	    $delUnit=$unit->delUnitKerja($id);
	    echo $delUnit;
	}
	
	function editAction()
	{
	    // Title Browser
	    $this->view->title = "Edit Data Unit Kerja";
	    // Navbar
	    $this->_helper->navbar('unit',0,0,0,0);
	    // get data
	    $id=$this->_request->get('id');
	    $unit=new UnitKerja();
	    $getUnit=$unit->getUnitKerjaById($id);
	    if(!$getUnit){
	        $this->view->eksis="f";
	    }else{
	        foreach ($getUnit as $data){
	            $this->view->id=$data['id_unit'];
	            $this->view->nm=$data['nm_unit'];
	        }
	    }
	    
	}
	
	function aupdAction(){
	    // disable layout
	    $this->_helper->layout->disableLayout();
	    // start inserting
	    $request = $this->getRequest()->getPost();
	    $id_unit= $request['id'];
	    $nm_unit = $this->_helper->string->esc_quote(trim($request['nm']));
	    $aktif= "t";
	    // validation
	    $err=0;
	    $msg="";
	    $vd = new Validation();
	    if($vd->validasiLength($nm_unit,1,50)=='F'){
	        $err++;
	        $msg=$msg."<strong>- Nama unit tidak boleh kosong maksimal 50 huruf</strong><br>";
	    }
	    if($err==0){
	        $unit = new UnitKerja();
	        $updUnit = $unit->updUnitKerja($nm_unit, $aktif, $id_unit);
	        echo $updUnit;
	    }else{
	        echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
	    }
	}
	
}