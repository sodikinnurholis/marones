<?php
/*
	Programmer	: Tiar Aristian
	Release		: Januari 2016
	Module		: Home Controller -> Controller untuk modul home
*/
include 'MasterController.php';

class HomeController extends MasterController
{
	function init()
	{
	    parent::init();
		$this->initView();
		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('Zend_Session');
		Zend_Loader::loadClass('Zend_Layout');
		$auth = Zend_Auth::getInstance();
		$ses_hrd = new Zend_Session_Namespace('ses_hrd');
		if (($auth->hasIdentity())and($ses_hrd->uname)) {
			$this->view->namauser =Zend_Auth::getInstance()->getIdentity()->nm_sdm;
			$this->view->username=Zend_Auth::getInstance()->getIdentity()->username;
		}else{
			$this->_redirect('/');
		}
		// layout
		$this->_helper->layout()->setLayout('main');
	}

	function indexAction()
	{
		// Title Browser
		$this->view->title = "Beranda SI.HRD";
		// navigation
		$this->_helper->navbar(0,0,0,0,0);
		
	}

	function aksesAction()
	{
		// Title Browser
		$this->view->title = "Halaman Alih SI.Akademik";
		$this->view->desc = "Tidak ada akses untuk user";
		// navigation
		$this->_helper->navbar('home',0,0,0,0);
	}
}