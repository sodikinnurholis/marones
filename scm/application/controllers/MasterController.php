<?php
/*
    Vendor      : Cebersoft
    Release     : Oktober 2016
    Module      : Master Controller -> Controller parent for all
*/

class MasterController extends Zend_Controller_Action
{
    public $username;

    function init()
    {

        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        $this->view->publicUrl = Zend_Registry::get('PUBLIC_URL');

        Zend_Loader::loadClass('Validation');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');

        $auth = Zend_Auth::getInstance();
        $ses_hrd = new Zend_Session_Namespace('ses_hrd');
        if (($auth->hasIdentity())and($ses_hrd->uname)) {
            $this->view->namauser =Zend_Auth::getInstance()->getIdentity()->nm_sdm;
            $this->view->username=Zend_Auth::getInstance()->getIdentity()->username;
            // global var
            $this->username=$ses_hrd->uname;
        } else {
            $this->_redirect('/');
        }
    }
}