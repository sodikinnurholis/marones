<?php
/*
 Programmer	: Tiar Aristian
 Release		: Januari 2016
 Module		: Home Controller -> Controller untuk modul home
 */
include 'MasterController.php';

class JenisskController extends MasterController
{
    function init()
    {
        parent::init();
        $this->initView();
        $this->view->baseUrl = $this->_request->getBaseUrl();
        Zend_Loader::loadClass('JenisSk');
        Zend_Loader::loadClass('Zend_Session');
        Zend_Loader::loadClass('Zend_Layout');
        $auth = Zend_Auth::getInstance();
        $ses_hrd = new Zend_Session_Namespace('ses_hrd');
        if (($auth->hasIdentity())and($ses_hrd->uname)) {
            $this->view->namauser =Zend_Auth::getInstance()->getIdentity()->nm_sdm;
            $this->view->username=Zend_Auth::getInstance()->getIdentity()->username;
        }else{
            $this->_redirect('/');
        }
        // layout
        $this->_helper->layout()->setLayout('main');
        // treeview
        $this->view->active_mst="active";
        $this->view->active_jenissk="active";
        
    }
    
    function indexAction()
    {
        // Title Browser
        $this->view->title = "Jenis Sk";
        // navigation
        $this->_helper->navbar(0,0,'jenissk/new',0,0);
        $jenissk= new JenisSk();
        $this->view->listJenissk=$jenissk->fetchAll();
    }
    function newAction(){
        // Title Browser
        $this->view->title = "Input Data Jenis SK";
        // navigation
        $this->_helper->navbar(0,'jenissk',0,0,0);
        // kategori ruangan
        $katJenissk = new JenisSk();
        $this->view->listJenissk=$katJenissk->fetchAll();
    }
    
    function ainsAction()
    {
        // disabel layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $jnssk = $this->_helper->string->esc_quote(trim($request['jenissk']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($jnssk,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Jenis sk tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $jenissk = new JenisSk();
            $setJenissk=$jenissk->setJenissk($jnssk);
            echo $setJenissk;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
    function adelAction()
    {
        // disabel layout
        $this->_helper->layout->disableLayout();
        // set database
        $request = $this->getRequest()->getPost();
        // gets value from ajax request
        $param = $request['param'];
        $id = $param[0];
        $jenissk = new JenisSk();
        $delJenissk=$jenissk->delJenissk($id);
        echo $delJenissk;
    }
    
    function editAction()
    {
        // Title Browser
        $this->view->title = "Edit Data Jenis SK";
        // Navbar
        $this->_helper->navbar('jenissk',0,0,0,0);
        // get data
        $id=$this->_request->get('id');
        $jenissk=new JenisSk();
        $getJns=$jenissk->getJenisskById($id);
        if(!$getJns){
            $this->view->eksis="f";
        }else{
            foreach ($getJns as $data){
                $this->view->id=$data['id_jns_sk'];
                $this->view->jenissk=$data['jenis_sk'];
            }
        }
    }
    
    function aupdAction()
    {
        // disabel layout
        $this->_helper->layout->disableLayout();
        // start inserting
        $request = $this->getRequest()->getPost();
        $id_jns_sk = $request['id'];
        $jnssk = $this->_helper->string->esc_quote(trim($request['jenissk']));
        // validation
        $err=0;
        $msg="";
        $vd = new Validation();
        if($vd->validasiLength($jnssk,1,50)=='F'){
            $err++;
            $msg=$msg."<strong>- Jenis sk tidak boleh kosong maksimal 50 huruf</strong><br>";
        }
        if($err==0){
            $jenissk = new JenisSk();
            $setJenissk=$jenissk->updJenissk($jnssk, $id_jns_sk);
            echo $setJenissk;
        }else{
            echo "F|Terjadi ".$err." kesalahan data input :<br>".$msg;
        }
    }
    
}