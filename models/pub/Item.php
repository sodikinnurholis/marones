<?php
class Item extends Zend_Db_Table
{
    protected $_name = 'v_item';
    protected $_primary='id_item';
    
    function getItemById($id_item){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from f_item_fby_id('$id_item')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setItem($nm_item,$kd_item,$id_item_class,$keterangan,$id_unit_def){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_item_ins('$nm_item','$kd_item','$id_item_class','$keterangan','$id_unit_def')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_item_ins'];
        }
        return $return;
    }
    function updItem($nm_item,$kd_item,$id_item_class,$keterangan,$id_unit_def,$id_item){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_item_upd('$nm_item','$kd_item','$id_item_class','$keterangan','$id_unit_def','$id_item')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_item_upd'];
        }
        return $return;
    }
    
    function delItem($id_item){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_item_del('$id_item')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_item_del'];
        }
        return $return;
    }
    
}