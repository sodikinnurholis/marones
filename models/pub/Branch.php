<?php
class Branch extends Zend_Db_Table
{
    protected $_name = 'v_branch';
    protected $_primary='id_branch';
    
    function getBranchById($id_branch){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from f_branch_fby_id('$id_branch')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setBranch($id_type_branch,$id_kontak){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_branch_ins('$id_type_branch','$id_kontak')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_branch_ins'];
        }
        return $return;
    }
    function updBranch($id_type_branch,$id_kontak,$id_branch){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_branch_upd('$id_type_branch','$id_kontak','$id_branch')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_branch_upd'];
        }
        return $return;
    }
    
    function delBranch($id_branch){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_branch_del('$id_branch')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_branch_del'];
        }
        return $return;
    }
    
}