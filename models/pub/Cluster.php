<?php
class Cluster extends Zend_Db_Table
{
    protected $_name = 'v_cluster';
    protected $_primary='id_cluster';
    
    function getClusterById($id_cluster){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from f_cluster_fby_id('$id_cluster')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setCluster($nm_cluster){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_cluster_ins('$nm_cluster')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_cluster_ins'];
        }
        return $return;
    }
    function updCluster($nm_cluster,$id_cluster){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_cluster_upd('$nm_cluster','$id_cluster')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_cluster_upd'];
        }
        return $return;
    }
    
    function delCluster($id_cluster){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_cluster_del('$id_cluster')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_cluster_del'];
        }
        return $return;
    }
    
}