<?php
class Unitkonv extends Zend_Db_Table
{
    protected $_name = 'v_unit_konv';
    protected $_primary='id_unit_from';
    
    function getUnitkonvById($id_unit_from,$id_unit_to){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from f_unit_konv_fby_id('$id_unit_from','$id_unit_to')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setUnitkonv($id_unit_from,$id_unit_to,$konv){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_unit_konv_ins('$id_unit_from','$id_unit_to',$konv)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_unit_konv_ins'];
        }
        return $return;
    }
    function updUnitkonv($id_unit_from,$id_unit_to,$konv){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_unit_konv_upd('$id_unit_from','$id_unit_to',$konv)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_unit_konv_upd'];
        }
        return $return;
    }
    
    function delUnitkonv($id_unit_from,$id_unit_to){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_unit_konv_del('$id_unit_from','$id_unit_to')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_unit_konv_del'];
        }
        return $return;
    }
    
}