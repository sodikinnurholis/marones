<?php
class Unit extends Zend_Db_Table
{
    protected $_name = 'unit';
    protected $_primary='id_unit';
    
    function getUnitById($id_unit){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from f_unit_fby_id('$id_unit')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setUnit($nm_unit,$id_kategori_unit){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_unit_ins('$nm_unit','$id_kategori_unit')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_unit_ins'];
        }
        return $return;
    }
    function updUnit($nm_unit,$id_kategori_unit,$id_unit){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_unit_upd('$nm_unit','$id_kategori_unit','$id_unit')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_unit_upd'];
        }
        return $return;
    }
    
    function delUnit($id_unit){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_unit_del('$id_unit')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_unit_del'];
        }
        return $return;
    }
    
}