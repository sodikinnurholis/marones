<?php
class Workflow extends Zend_Db_Table
{
    protected $_name = 'v_workflow';
    protected $_primary='id_workflow';
    
    function getWorkflowById($id_workflow){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from f_workflow_fby_id('$id_workflow')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setWorkflow($nm_workflow){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_workflow_ins('$nm_workflow')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_workflow_ins'];
        }
        return $return;
    }
    function updWorkflow($nm_workflow,$id_workflow){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_workflow_upd('$nm_workflow','$id_workflow')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_workflow_upd'];
        }
        return $return;
    }
    
    function delWorkflow($id_workflow){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_workflow_del('$id_workflow')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_workflow_del'];
        }
        return $return;
    }
    
}