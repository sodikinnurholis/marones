<?php
class Rumah extends Zend_Db_Table
{
    protected $_name = 'v_rumah';
    protected $_primary='id_rumah';
    
    function getRumahById($id_rumah){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from f_rumah_fby_id('$id_rumah')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setRumah($nm_rumah,$no_rumah,$harga,$id_cluster){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_rumah_ins('$nm_rumah','$no_rumah',$harga,'$id_cluster')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_rumah_ins'];
        }
        return $return;
    }
    function updRumah($nm_rumah,$no_rumah,$harga,$id_cluster,$id_rumah){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_rumah_upd('$nm_rumah','$no_rumah',$harga,'$id_cluster','$id_rumah')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_rumah_upd'];
        }
        return $return;
    }
    
    function delRumah($id_rumah){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_rumah_del('$id_rumah')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_rumah_del'];
        }
        return $return;
    }
    
}