<?php
class ItemClass extends Zend_Db_Table
{
    protected $_name = 'v_item_class';
    protected $_primary='id_item_class';
    
    function getItemClassById($id_item_class){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from f_item_class_fby_id('$id_item_class')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setItemClass($nm_class,$a_stock){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_item_class_ins('$nm_class','$a_stock')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_item_class_ins'];
        }
        return $return;
    }
    function updItemClass($nm_class,$a_stock,$id_item_class){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_item_class_upd('$nm_class','$a_stock','$id_item_class')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_item_class_upd'];
        }
        return $return;
    }
    
    function delItemClass($id_item_class){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_item_class_del('$id_item_class')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_item_class_del'];
        }
        return $return;
    }
    
}