<?php
class Kontak extends Zend_Db_Table
{
    protected $_name = 'v_kontak';
    protected $_primary='id_kontak';
    
    function getKontakById($id_kontak){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from f_kontak_fby_id('$id_kontak')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setKontak($nm_kontak,$id_type_kontak,$tlp,$email,$alamat){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_kontak_ins('$nm_kontak','$id_type_kontak','$tlp','$email','$alamat')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_kontak_ins'];
        }
        return $return;
    }
    function updKontak($nm_kontak,$id_type_kontak,$tlp,$email,$alamat,$id_kontak){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_kontak_upd('$nm_kontak','$id_type_kontak','$tlp','$email','$alamat','$id_kontak')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_kontak_upd'];
        }
        return $return;
    }
    
    function delKontak($id_kontak){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_kontak_del('$id_kontak')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_kontak_del'];
        }
        return $return;
    }
    
}