<?php
class RealisasiMaterial extends Zend_Db_Table
{
    protected $_name = 'prd.v_realisasi_material';
    protected $_primary='id_realisasi_material';
    
    function getRealisasiMaterialtById($id_realisasi_material){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from prd.f_realisasi_material_fby_id('$id_realisasi_material')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setRealisasiMaterial($tanggal_realisasi_material,$id_spk_material,$qty_realisasi){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_realisasi_material_ins('$tanggal_realisasi_material','$id_spk_material',$qty_realisasi)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_realisasi_material_ins'];
        }
        return $return;
    }
    function updRealisasiMaterial($tanggal_realisasi_material,$id_spk_material,$qty_realisasi,$id_realisasi_material){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_realisasi_material_upd('$tanggal_realisasi_material','$id_spk_material',$qty_realisasi,'$id_realisasi_material')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_realisasi_material_upd'];
        }
        return $return;
    }
    
    function delRealisasiMaterial($id_realisasi_material){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_realisasi_material_del('$id_realisasi_material')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_realisasi_material_del'];
        }
        return $return;
    }
    
}