<?php
class RealisasiSubkon extends Zend_Db_Table
{
    protected $_name = 'prd.v_realisasi_subkon';
    protected $_primary='id_realisasi_subkon';
    
    function getRealisasiSubkontById($id_realisasi_subkon){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from prd.f_realisasi_subkon_fby_id('$id_realisasi_subkon')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setRealisasiSubkon($id_spk_subkon,$tanggal_kontrabon,$no_kontrabon,$nominal_kontrabon){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_realisasi_subkon_ins('$id_spk_subkon','$tanggal_kontrabon','$no_kontrabon',$nominal_kontrabon)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_realisasi_subkon_ins'];
        }
        return $return;
    }
    function updRealisasiSubkon($id_spk_subkon,$tanggal_kontrabon,$no_kontrabon,$nominal_kontrabon,$id_realisasi_subkon){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_realisasi_subkon_upd('$id_spk_subkon','$tanggal_kontrabon','$no_kontrabon',$nominal_kontrabon,'$id_realisasi_subkon')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_realisasi_subkon_upd'];
        }
        return $return;
    }
    
    function delRealisasiSubkon($id_realisasi_subkon){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_realisasi_subkon_del('$id_realisasi_subkon')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_realisasi_subkon_del'];
        }
        return $return;
    }
    
}