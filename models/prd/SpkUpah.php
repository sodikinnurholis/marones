<?php
class SpkUpah extends Zend_Db_Table
{
    protected $_name = 'prd.v_spk_upah';
    protected $_primary='id_spk_upah';
    
    function getSpkUpahtById($id_spk_upah){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from prd.f_spk_upah_fby_id('$id_spk_upah')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function getSpkUpahtBySpkWorkflow($id_spk_workflow){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from prd.f_spk_upah_fby_spk_workflow('$id_spk_workflow')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setSpkUpah($id_spk_workflow,$id_upah,$tarif){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_upah_ins('$id_spk_workflow','$id_upah',$tarif)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_upah_ins'];
        }
        return $return;
    }
    function updSpkUpah($id_spk_workflow,$id_upah,$tarif,$id_spk_upah){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_upah_upd('$id_spk_workflow','$id_upah',$tarif,'$id_spk_upah')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_upah_upd'];
        }
        return $return;
    }
    
    function delSpkUpah($id_spk_upah){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_upah_del('$id_spk_upah')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_upah_del'];
        }
        return $return;
    }
    
}