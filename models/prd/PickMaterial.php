<?php
class PickMaterial extends Zend_Db_Table
{
    protected $_name = 'prd.v_pick_material';
    protected $_primary='id_pick_material';
    
    function getPickMaterialtById($id_pick_material){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from prd.f_pick_material_fby_id('$id_pick_material')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setPickMaterial($tanggal_pick_material,$id_realisasi_material,$id_ri_item,$qty_pick){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_pick_material_ins('$tanggal_pick_material','$id_realisasi_material','$id_ri_item',$qty_pick)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_pick_material_ins'];
        }
        return $return;
    }
    function updPickMaterial($tanggal_pick_material,$id_realisasi_material,$id_ri_item,$qty_pick,$id_pick_material){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_pick_material_upd('$tanggal_pick_material','$id_ri_item','$id_realisasi_material',$qty_pick,'$id_pick_material')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_pick_material_upd'];
        }
        return $return;
    }
    
    function delPickMaterial($id_pick_material){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_pick_material_del('$id_pick_material')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_pick_material_del'];
        }
        return $return;
    }
    
}