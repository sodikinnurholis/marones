<?php
class SpkSubkon extends Zend_Db_Table
{
    protected $_name = 'prd.v_spk_subkon';
    protected $_primary='id_spk_subkon';
    
    function getSpkSubkontById($id_spk_subkon){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from prd.f_spk_subkon_fby_id('$id_spk_subkon')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function getSpkSubkontBySpkWorkflow($id_spk_workflow){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from prd.f_spk_subkon_fby_spk_workflow('$id_spk_workflow')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setSpkSubkon($id_spk_workflow,$id_subkon,$tarif){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_subkon_ins('$id_spk_workflow','$id_subkon',$tarif)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_subkon_ins'];
        }
        return $return;
    }
    function updSpkSubkon($id_spk_workflow,$id_subkon,$tarif,$id_spk_subkon){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_subkon_upd('$id_spk_workflow','$id_subkon',$tarif,'$id_spk_subkon')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_subkon_upd'];
        }
        return $return;
    }
    
    function delSpkSubkon($id_spk_subkon){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_subkon_del('$id_spk_subkon')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_subkon_del'];
        }
        return $return;
    }
    
}