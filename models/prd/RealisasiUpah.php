<?php
class RealisasiUpah extends Zend_Db_Table
{
    protected $_name = 'prd.v_realisasi_upah';
    protected $_primary='id_realisasi_upah';
    
    function getRealisasiUpahtById($id_realisasi_upah){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from prd.f_realisasi_upah_fby_id('$id_realisasi_upah')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setRealisasiUpah($id_spk_upah,$tanggal_kontrabon,$no_kontrabon,$nominal_kontrabon){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_realisasi_upah_ins('$id_spk_upah','$tanggal_kontrabon','$no_kontrabon',$nominal_kontrabon)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_realisasi_upah_ins'];
        }
        return $return;
    }
    function updRealisasiUpah($id_spk_upah,$tanggal_kontrabon,$no_kontrabon,$nominal_kontrabon,$id_realisasi_upah){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_realisasi_upah_upd('$id_spk_upah','$tanggal_kontrabon','$no_kontrabon',$nominal_kontrabon,'$id_realisasi_upah')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_realisasi_upah_upd'];
        }
        return $return;
    }
    
    function delRealisasiUpah($id_realisasi_upah){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_realisasi_upah_del('$id_realisasi_upah')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_realisasi_upah_del'];
        }
        return $return;
    }
    
}