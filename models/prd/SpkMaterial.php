<?php
class SpkMaterial extends Zend_Db_Table
{
    protected $_name = 'prd.v_spk_material';
    protected $_primary='id_spk_material';
    
    function getSpkMaterialtById($id_spk_material){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from prd.f_spk_material_fby_id('$id_spk_material')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function getSpkMaterialtBySpkWorkflow($id_spk_workflow){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from prd.f_spk_material_fby_spk_workflow('$id_spk_workflow')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setSpkMaterial($id_spk_workflow,$id_item,$qty){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_material_ins('$id_spk_workflow','$id_item',$qty)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_material_ins'];
        }
        return $return;
    }
    function updSpkMaterial($id_spk_workflow,$id_item,$qty,$id_spk_material){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_material_upd('$id_spk_workflow','$id_item',$qty,'$id_spk_material')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_material_upd'];
        }
        return $return;
    }
    
    function delSpkMaterial($id_spk_material){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_material_del('$id_spk_material')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_material_del'];
        }
        return $return;
    }
    
}