<?php
class BayarRealisasiUpah extends Zend_Db_Table
{
    protected $_name = 'prd.v_bayar_realisasi_upah';
    protected $_primary='id_bayar_realisasi_upah';
    
    function getBayarRealisasiUpahtById($id_bayar_realisasi_upah){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from prd.f_bayar_realisasi_upah_fby_id('$id_bayar_realisasi_upah')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setBayarRealisasiUpah($tanggal_bayar_realisasi_upah,$id_realisasi_upah,$nominal_bayar){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_bayar_realisasi_upah_ins('$tanggal_bayar_realisasi_upah','$id_realisasi_upah',$nominal_bayar)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_realisasi_upah_ins'];
        }
        return $return;
    }
    function updBayarRealisasiUpah($tanggal_bayar_realisasi_upah,$id_realisasi_upah,$nominal_bayar,$id_bayar_realisasi_upah){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_bayar_realisasi_upah_upd('$tanggal_bayar_realisasi_upah','$id_realisasi_upah',$nominal_bayar,'$id_bayar_realisasi_upah')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_realisasi_upah_upd'];
        }
        return $return;
    }
    
    function delBayarRealisasiUpah($id_bayar_realisasi_upah){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_bayar_realisasi_upah_del('$id_bayar_realisasi_upah')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_realisasi_upah_del'];
        }
        return $return;
    }
    
}