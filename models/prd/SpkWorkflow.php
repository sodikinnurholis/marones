<?php
class SpkWorkflow extends Zend_Db_Table
{
    protected $_name = 'prd.v_spk_workflow';
    protected $_primary='id_spk_workflow';
    
    function getSpkWorkflowtById($id_spk_workflow){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from prd.f_spk_workflow_fby_id('$id_spk_workflow')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setSpkWorkflow($id_spk,$id_workflow,$urutan){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_workflow_ins('$id_spk','$id_workflow',$urutan)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_workflow_ins'];
        }
        return $return;
    }
    function updSpkWorkflow($id_spk,$id_workflow,$urutan,$id_spk_workflow){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_workflow_upd('$id_spk','$id_workflow',$urutan,'$id_spk_workflow')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_workflow_upd'];
        }
        return $return;
    }
    
    function delSpkWorkflow($id_spk_workflow){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_workflow_del('$id_spk_workflow')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_workflow_del'];
        }
        return $return;
    }
    
}