<?php
class Spk extends Zend_Db_Table
{
    protected $_name = 'prd.v_spk';
    protected $_primary='id_spk';
    
    function getSpktById($id_spk){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from prd.f_spk_fby_id('$id_spk')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setSpk($tgl_spk,$id_so){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_ins('$tgl_spk','$id_so')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_ins'];
        }
        return $return;
    }
    function updSpk($tgl_spk,$id_so,$id_spk){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_upd('$tgl_spk','$id_so','$id_spk')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_upd'];
        }
        return $return;
    }
    
    function delSpk($id_spk){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from prd.f_spk_del('$id_spk')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_spk_del'];
        }
        return $return;
    }
    
}