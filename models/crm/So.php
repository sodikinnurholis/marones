<?php
class So extends Zend_Db_Table
{
    protected $_name = 'crm.v_so';
    protected $_primary='id_so';

    function to_pg_array($set) {
        settype($set, 'array');
        $result = array();
        foreach ($set as $t) {
            if (is_array($t)) {
                $result[] = to_pg_array($t);
            } else {
                $t = str_replace('"', '\\"', $t);
                if (! is_numeric($t))
                    $t = '"' . $t . '"';
                    $result[] = $t;
            }
        }
        return '{' . implode(",", $result) . '}';
    }
    function querySo($id_customer,$closed,$tgl1,$tgl2){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $id_customer=$this->to_pg_array($id_customer);
        $closed=$this->to_pg_array($closed);
        if(($tgl1=='')or($tgl2=='')){
            $stmt=$db->query("select * from  crm.f_so_query('$id_customer','$closed','01-01-01','01-01-01')");
        }else{
            $stmt=$db->query("select * from  crm.f_so_query('$id_customer','$closed','$tgl1','$tgl2')");
        }
        $data=$stmt->fetchAll();
        return $data;
    }
    
    function getSoById($id_so){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from crm.f_so_fby_id('$id_so')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setSo($tgl_so,$keterangan,$booking_v,$uang_muka,$id_customer,$id_rumah){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from crm.f_so_ins('$tgl_so','$keterangan',$booking_v,$uang_muka,'$id_customer','$id_rumah')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_so_ins'];
        }
        return $return;
    }
    function updSo($tgl_so,$keterangan,$booking_v,$uang_muka,$id_customer,$id_rumah,$id_so){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from crm.f_so_upd('$tgl_so','$keterangan',$booking_v,$uang_muka,'$id_customer','$id_rumah','$id_so')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_so_upd'];
        }
        return $return;
    }
    
    function delSo($id_so){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from crm.f_so_del('$id_so')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_so_del'];
        }
        return $return;
    }
    
}