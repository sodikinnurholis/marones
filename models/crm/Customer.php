<?php
class Customer extends Zend_Db_Table
{
    protected $_name = 'crm.v_customer';
    protected $_primary='id_customer';
    
    function getCustomerById($id_customer){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from crm.f_customer_fby_id('$id_customer')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setCustomer($nm_customer,$nik,$kontak,$email){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from crm.f_customer_ins('$nm_customer','$nik','$kontak','$email')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_customer_ins'];
        }
        return $return;
    }
    function updCustomer($nm_customer,$nik,$kontak,$email,$id_customer){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from crm.f_customer_upd('$nm_customer','$nik','$kontak','$email','$id_customer')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_customer_upd'];
        }
        return $return;
    }
    
    function delCustomer($id_customer){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from crm.f_customer_del('$id_customer')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_customer_del'];
        }
        return $return;
    }
    
}