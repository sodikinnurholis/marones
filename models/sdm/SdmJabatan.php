<?php
class SdmJabatan extends Zend_Db_Table
{
    protected $_name = 'sdm.v_sdm_jabatan';
    protected $_primary='id_sdm_jabatan';
    
    function getSdmJabatanBySdm($id_sdm){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from sdm.f_sdm_jabatan_filter_by_sdm('$id_sdm')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function getSdmJabatanByUnit($id_unit){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from sdm.f_sdm_jabatan_filter_by_unit('$id_unit')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setSdmJabatan($id_sdm,$id_jabatan){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from sdm.f_sdm_jabatan_ins('$id_sdm','$id_jabatan')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_sdm_jabatan_ins'];
        }
        return $return;
    }
    function updSdmJabatan($id_sdm,$id_jabatan,$id_sdm_jabatan){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from sdm.f_sdm_jabatan_upd('$id_sdm','$id_jabatan','$id_sdm_jabatan')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_sdm_jabatan_upd'];
        }
        return $return;
    }
    
    function delSdmJabatan($id_sdm_jabatan){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from sdm.f_sdm_jabatam_del('$id_sdm_jabatan')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_sdm_jabatam_del'];
        }
        return $return;
    }
}