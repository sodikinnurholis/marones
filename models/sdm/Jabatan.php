<?php
class Jabatan extends Zend_Db_Table
{
    protected $_name = 'sdm.v_jabatan';
    protected $_primary='id_jabatan';
    
    function getJabatanByid($id_jabatan){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from sdm.f_jabatan_filter_by_id('$id_jabatan')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function getJabatanByUnit($id_unit){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from sdm.f_jabatan_filter_by_unit('$id_unit')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setJabatan($id_unit,$nm_jabatan,$superadmin,$level){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from sdm.f_sdm_jabatan_ins('$id_unit','$nm_jabatan','$superadmin',$level)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_sdm_jabatan_ins'];
        }
        return $return;
    }
    function updJabatan($id_unit,$nm_jabatan,$superadmin,$level,$id_jabatan){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from sdm.f_sdm_jabatan_upd('$id_unit','$nm_jabatan','$superadmin',$level,'$id_jabatan')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_sdm_jabatan_upd'];
        }
        return $return;
    }
    
    function delJabatan($id_jabatan){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from sdm.f_sdm_jabatan_del('$id_jabatan')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_sdm_jabatan_del'];
        }
        return $return;
    }
}