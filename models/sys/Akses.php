<?php
class Akses extends Zend_Db_Table
{    
    function setAkses($id_jabatan,$id_modul){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from sys.f_akses_ins('$id_jabatan','$id_modul')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_rtn_ins'];
        }
        return $return;
    }
    
    function delAkses($id_akses){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from sys.f_akses_del('$id_akses')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_akses_del'];
        }
        return $return;
    }
    function getAksesAksiByJabatan($id_jabatan){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from sys.f_akses_aksi_query_by_jabatan('$id_jabatan')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_akses_aksi_query_by_jabatan'];
        }
        return $return;
    }
    function getAksesByJabatan($id_jabatan){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from sys.f_akses_query_by_jabatan('$id_jabatan')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_akses_query_by_jabatan'];
        }
        return $return;
    }
    
}