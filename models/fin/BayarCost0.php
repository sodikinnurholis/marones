<?php
class BayarCost0 extends Zend_Db_Table
{
    protected $_name = 'fin.v_bayar_cost0';
    protected $_primary='id_bayar0';
    
    function getBayarCost0tById($id_bayar0){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from fin.f_bayar_cost0_fby_id('$id_bayar0')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setBayarCost0($tgl_bayar,$id_kas,$no_bukti,$keterangan,$status){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_bayar_cost0_ins('$tgl_bayar','$id_kas','$no_bukti','$keterangan',$status)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_cost0_ins'];
        }
        return $return;
    }
    function updBayarCost0($tgl_bayar,$id_kas,$no_bukti,$keterangan,$status,$id_bayar0){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_bayar_cost0_upd('$tgl_bayar','$id_kas','$no_bukti','$keterangan',$status,'$id_bayar0')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_cost0_upd'];
        }
        return $return;
    }
    
    function delBayarCost0($id_bayar0){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_bayar_cost0_del('$id_bayar0')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_cost0_del'];
        }
        return $return;
    }
    
}