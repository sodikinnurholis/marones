<?php
class BayarInvoice0 extends Zend_Db_Table
{
    protected $_name = 'fin.v_bayar_invoice0';
    protected $_primary='id_bayar0';
    
    function getBayarInvoice0tById($id_bayar0){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from fin.f_bayar_invoice0_fby_id('$id_bayar0')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setBayarInvoice0($tgl_bayar,$id_supplier,$id_kas,$keterangan,$status){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_bayar_invoice0_ins('$tgl_bayar','$id_supplier','$id_kas','$keterangan',$status)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_invoice0_ins'];
        }
        return $return;
    }
    function updBayarInvoice0($tgl_bayar,$id_supplier,$id_kas,$keterangan,$status,$id_bayar0){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_bayar_invoice0_upd('$tgl_bayar','$id_supplier','$id_kas','$keterangan',$status,'$id_bayar0')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_invoice0_upd'];
        }
        return $return;
    }
    
    function delBayarInvoice0($id_bayar0){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_bayar_invoice0_del('$id_bayar0')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_invoice0_del'];
        }
        return $return;
    }
    
}