<?php
class Debitur extends Zend_Db_Table
{
    protected $_name = 'fin.v_debitur';
    protected $_primary='id_debitur';
    
    function getDebiturById($id_debitur){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from fin.f_debitur_fby_id('$id_debitur')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setDebitur($id_kontak){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_debitur_ins('$id_kontak')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_debitur_ins'];
        }
        return $return;
    }
    function updDebitur($id_kontak,$id_debitur){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_debitur_upd('$id_kontak','$id_debitur')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_debitur_upd'];
        }
        return $return;
    }
    
    function delDebitur($id_debitur){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_debitur_del('$id_debitur')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_debitur_del'];
        }
        return $return;
    }
    
}