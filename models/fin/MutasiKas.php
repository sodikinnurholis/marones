<?php
class MutasiKas extends Zend_Db_Table
{
    protected $_name = 'fin.v_mutasi_kas';
    protected $_primary='id_mutasi_kas';
    
    function to_pg_array($set) {
        settype($set, 'array');
        $result = array();
        foreach ($set as $t) {
            if (is_array($t)) {
                $result[] = to_pg_array($t);
            } else {
                $t = str_replace('"', '\\"', $t);
                if (! is_numeric($t))
                    $t = '"' . $t . '"';
                    $result[] = $t;
            }
        }
        return '{' . implode(",", $result) . '}';
    }
    
    function queryMutasiKas($id_kas_from,$id_kas_to,$tgl1,$tgl2){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $id_kas_from=$this->to_pg_array($id_kas_from);
        $id_kas_to=$this->to_pg_array($id_kas_to);
         if(($tgl1=='')or($tgl2=='')){
            $stmt=$db->query("select * from  fin.f_mutasi_kas_query('$id_kas_from','$id_kas_to','01-01-01','01-01-01')");
        }else{
            $stmt=$db->query("select * from  fin.f_mutasi_kas_query('$id_kas_from','$id_kas_to','$tgl1','$tgl2')");
        }
        $data=$stmt->fetchAll();
        return $data;
    }

    function getMutasiKasById($id_mutasi_kas){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from fin.f_mutasi_kas_fby_id('$id_mutasi_kas')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setMutasiKas($tgl_mutasi,$id_kas_from,$id_kas_to,$nominal,$keterangan,$no_bukti,$status){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_mutasi_kas_ins('$tgl_mutasi','$id_kas_from','$id_kas_to',$nominal,'$keterangan','$no_bukti',$status)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_mutasi_kas_ins'];
        }
        return $return;
    }
    function updMutasiKas($tgl_mutasi,$id_kas_from,$id_kas_to,$nominal,$keterangan,$no_bukti,$status,$id_mutasi_kas){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_mutasi_kas_upd('$tgl_mutasi','$id_kas_from','$id_kas_to',$nominal,'$keterangan','$no_bukti',$status,'$id_mutasi_kas')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_mutasi_kas_upd'];
        }
        return $return;
    }
    
    function delMutasiKas($id_mutasi_kas){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_mutasi_kas_del('$id_mutasi_kas')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_mutasi_kas_del'];
        }
        return $return;
    }
    
}