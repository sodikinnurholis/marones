<?php
class SaKas extends Zend_Db_Table
{
    protected $_name = 'fin.v_sa_kas';
    protected $_primary='id_sa_kas';
    
    function to_pg_array($set) {
        settype($set, 'array');
        $result = array();
        foreach ($set as $t) {
            if (is_array($t)) {
                $result[] = to_pg_array($t);
            } else {
                $t = str_replace('"', '\\"', $t);
                if (! is_numeric($t))
                    $t = '"' . $t . '"';
                    $result[] = $t;
            }
        }
        return '{' . implode(",", $result) . '}';
    }
    
    function querySaKas($id_kas,$id_branch,$id_tipe_kas,$tgl1,$tgl2){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $id_kas=$this->to_pg_array($id_kas);
        $id_branch=$this->to_pg_array($id_branch);
        $id_tipe_kas=$this->to_pg_array($id_tipe_kas);
         if(($tgl1=='')or($tgl2=='')){
            $stmt=$db->query("select * from  fin.f_sa_kas_query('$id_kas','$id_branch','$id_tipe_kas','01-01-01','01-01-01')");
        }else{
            $stmt=$db->query("select * from  fin.f_sa_kas_query('$id_kas','$id_branch','$id_tipe_kas','$tgl1','$tgl2')");
        }
        $data=$stmt->fetchAll();
        return $data;
    }

    function getSaKasById($id_kas){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from fin.f_sa_kas_fby_id('$id_kas')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setSaKas($id_kas,$nominal_sa,$tgl_saldo_awal){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_sa_kas_ins('$id_kas',$nominal_sa,'$tgl_saldo_awal')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_sa_kas_ins'];
        }
        return $return;
    }
    function updSaKas($nominal_sa,$tgl_saldo_awal,$id_kas){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_sa_kas_upd($nominal_sa,'$tgl_saldo_awal','$id_kas')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_sa_kas_upd'];
        }
        return $return;
    }
    
    function delSaKas($id_kas){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_sa_kas_del('$id_kas')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_sa_kas_del'];
        }
        return $return;
    }
    
}