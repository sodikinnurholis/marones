<?php
class BayarCost1 extends Zend_Db_Table
{
    protected $_name = 'fin.v_bayar_cost1';
    protected $_primary='id_bayar1';
    
    function getBayarCost1tById($id_bayar1){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from fin.f_bayar_cost1_fby_id('$id_bayar1')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setBayarCost1($id_bayar0,$id_cost,$nominal_bayar,$no_bon,$ket_bon){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_bayar_cost1_ins('$id_bayar0','$id_cost',$nominal_bayar,'$no_bon','$ket_bon')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_cost1_ins'];
        }
        return $return;
    }
    function updBayarCost1($id_bayar0,$id_cost,$nominal_bayar,$no_bon,$ket_bon,$id_bayar1){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_bayar_cost1_upd('$id_bayar0','$id_cost',$nominal_bayar,'$no_bon','$ket_bon','$id_bayar1')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_cost1_upd'];
        }
        return $return;
    }
    
    function delBayarCost1($id_bayar1){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_bayar_cost1_del('$id_bayar1')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_cost1_del'];
        }
        return $return;
    }
    
}