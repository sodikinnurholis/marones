<?php
class BayarInvoice1 extends Zend_Db_Table
{
    protected $_name = 'fin.v_bayar_invoice1';
    protected $_primary='id_bayar1';
    
    function getBayarInvoice1tById($id_bayar1){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from fin.f_bayar_invoice1_fby_id('$id_bayar1')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setBayarInvoice1($id_bayar0,$id_invoice0,$nominal_bayar){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_bayar_invoice1_ins('$id_bayar0','$id_invoice0',$nominal_bayar)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_invoice1_ins'];
        }
        return $return;
    }
    function updBayarInvoice1($id_bayar0,$id_invoice0,$nominal_bayar,$id_bayar1){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_bayar_invoice1_upd('$id_bayar0','$id_invoice0',$nominal_bayar,'$id_bayar1')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_invoice1_upd'];
        }
        return $return;
    }
    
    function delBayarInvoice1($id_bayar1){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_bayar_invoice1_del('$id_bayar1')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_bayar_invoice1_del'];
        }
        return $return;
    }
    
}