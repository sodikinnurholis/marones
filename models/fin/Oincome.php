<?php
class Oincome extends Zend_Db_Table
{
    protected $_name = 'fin.v_oincome';
    protected $_primary='id_oincome';
    
    function getOincomeById($id_oincome){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from fin.f_oincome_fby_id('$id_oincome')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setOincome($nm_oincome,$id_branch){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_oincome_ins('$nm_oincome','$id_branch')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_oincome_ins'];
        }
        return $return;
    }
    function updOincome($nm_oincome,$id_branch,$id_oincome){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_oincome_upd('$nm_oincome','$id_branch','$id_oincome')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_oincome_upd'];
        }
        return $return;
    }
    
    function delOincome($id_oincome){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_oincome_del('$id_oincome')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_oincome_del'];
        }
        return $return;
    }
    
}