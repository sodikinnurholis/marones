<?php
class PenerimaanLain extends Zend_Db_Table
{
    protected $_name = 'fin.v_penerimaan_lain';
    protected $_primary='id_penerimaan_lain';
    
    function to_pg_array($set) {
        settype($set, 'array');
        $result = array();
        foreach ($set as $t) {
            if (is_array($t)) {
                $result[] = to_pg_array($t);
            } else {
                $t = str_replace('"', '\\"', $t);
                if (! is_numeric($t))
                    $t = '"' . $t . '"';
                    $result[] = $t;
            }
        }
        return '{' . implode(",", $result) . '}';
    }
    
    function queryPenerimaanLain($id_kas,$tgl1,$tgl2){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $id_kas=$this->to_pg_array($id_kas);
         if(($tgl1=='')or($tgl2=='')){
            $stmt=$db->query("select * from  fin.f_penerimaan_lain_query('$id_kas','$','$01-01-01','01-01-01')");
        }else{
            $stmt=$db->query("select * from  fin.f_penerimaan_lain_query('$id_kas','$tgl1','$tgl2')");
        }
        $data=$stmt->fetchAll();
        return $data;
    }

    function getPenerimaanLainById($id_penerimaan_lain){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from fin.f_penerimaan_lain_fby_id('$id_penerimaan_lain')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setPenerimaanLain($id_penerimaan_lain,$no_penerimaan_lain,$tgl_penerimaan,$id_kas,$id_oincome,$keterangan,$nominal,$no_bukti,$status){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_penerimaan_lain_ins('$id_penerimaan_lain',$no_penerimaan_lain,'$tgl_penerimaan','$id_kas','$id_oincome','$keterangan','$nominal','$no_bukti','$status')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_penerimaan_lain_ins'];
        }
        return $return;
    }
    function updPenerimaanLain($no_penerimaan_lain,$tgl_penerimaan,$id_kas,$id_oincome,$keterangan,$nominal,$no_bukti,$status,$id_penerimaan_lain){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_penerimaan_lain_upd($no_penerimaan_lain,'$tgl_penerimaan','$id_kas','$id_oincome','$keterangan','$nominal','$no_bukti','$status','$id_penerimaan_lain')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_penerimaan_lain_upd'];
        }
        return $return;
    }
    
    function delPenerimaanLain($id_penerimaan_lain){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_penerimaan_lain_del('$id_penerimaan_lain')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_penerimaan_lain_del'];
        }
        return $return;
    }
    
}