<?php
class Kas extends Zend_Db_Table
{
    protected $_name = 'fin.v_kas';
    protected $_primary='id_kas';
    
    function getKasById($id_kas){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from fin.f_kas_fby_id('$id_kas')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setKas($nm_kas,$id_tipe_kas,$id_branch,$no_rek,$pemilik_rek){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_kas_ins('$nm_kas','$id_tipe_kas','$id_branch','$no_rek','$pemilik_rek')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_kas_ins'];
        }
        return $return;
    }
    function updKas($nm_kas,$id_tipe_kas,$id_branch,$no_rek,$pemilik_rek,$id_kas){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_kas_upd('$nm_kas','$id_tipe_kas','$id_branch','$no_rek','$pemilik_rek','$id_kas')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_kas_upd'];
        }
        return $return;
    }
    
    function delKas($id_kas){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_kas_del('$id_kas')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_kas_del'];
        }
        return $return;
    }
    
}