<?php
class Kreditur extends Zend_Db_Table
{
    protected $_name = 'fin.v_kreditur';
    protected $_primary='id_kreditur';
    
    function getKrediturById($id_kreditur){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from fin.f_kreditur_fby_id('$id_kreditur')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setKreditur($id_kontak){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_kreditur_ins('$id_kontak')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_kreditur_ins'];
        }
        return $return;
    }
    function updKreditur($id_kontak,$id_kreditur){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_kreditur_upd('$id_kontak','$id_kreditur')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_kreditur_upd'];
        }
        return $return;
    }
    
    function delKreditur($id_kreditur){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from fin.f_kreditur_del('$id_kreditur')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_kreditur_del'];
        }
        return $return;
    }
    
}