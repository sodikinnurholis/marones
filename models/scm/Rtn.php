<?php
class Rtn extends Zend_Db_Table
{
    protected $_name = 'scm.v_rtn';
    protected $_primary='id_rtn';
    
    function getRtntById($id_rtn){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from scm.f_rtn_fby_id('$id_rtn')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setRtn($id_ri,$no_rtn,$tgl_rtn,$status){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_rtn_ins('$id_ri','$no_rtn','$tgl_rtn',$status)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_rtn_ins'];
        }
        return $return;
    }
    function updRtn($id_ri,$no_rtn,$tgl_rtn,$status,$id_rtn){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_rtn_upd('$id_ri','$no_rtn','$tgl_rtn',$status,'$id_rtn')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_rtn_upd'];
        }
        return $return;
    }
    
    function delRtn($id_rtn){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_rtn_del('$id_rtn')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_rtn_del'];
        }
        return $return;
    }
    
}