<?php
class RiItem extends Zend_Db_Table
{
    protected $_name = 'scm.v_ri_item';
    protected $_primary='id_ri_item';
    
    function getRiItemtById($id_ri_item){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from scm.f_ri_item_fby_id('$id_ri_item')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function getRiItemtByRi($id_ri){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from scm.f_ri_item_fby_ri('$id_ri')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setRiItem($id_ri,$id_item,$qty_ri,$id_unit_ri,$price_ri,$konv_satuan,$id_gudang){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_ri_item_ins('$id_ri','$id_item',$qty_ri,'$id_unit_ri',$price_ri,$konv_satuan,'$id_gudang')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_ri_ins'];
        }
        return $return;
    }
    function updRiItem($id_ri,$id_item,$qty_ri,$id_unit_ri,$price_ri,$konv_satuan,$id_gudang,$id_ri_item){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_ri_item_upd('$id_ri','$id_item',$qty_ri,'$id_unit_ri',$price_ri,$konv_satuan,'$id_gudang','$id_ri_item')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_ri_item_upd'];
        }
        return $return;
    }
    
    function delRiItem($id_ri_item){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_ri_item_del('$id_ri_item')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_ri_item_del'];
        }
        return $return;
    }
    
}