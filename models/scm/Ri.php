<?php
class Ri extends Zend_Db_Table
{
    protected $_name = 'scm.v_ri';
    protected $_primary='id_ri';
    
    function to_pg_array($set) {
        settype($set, 'array');
        $result = array();
        foreach ($set as $t) {
            if (is_array($t)) {
                $result[] = to_pg_array($t);
            } else {
                $t = str_replace('"', '\\"', $t);
                if (! is_numeric($t))
                    $t = '"' . $t . '"';
                    $result[] = $t;
            }
        }
        return '{' . implode(",", $result) . '}';
    }
    
    function queryRi($id_supplier,$closed,$tgl1,$tgl2){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $id_supplier=$this->to_pg_array($id_supplier);
        $closed=$this->to_pg_array($closed);
         if(($tgl1=='')or($tgl2=='')){
            $stmt=$db->query("select * from  scm.f_ri_query('$id_supplier','$closed','01-01-01','01-01-01')");
        }else{
            $stmt=$db->query("select * from  scm.f_ri_query('$id_supplier','$closed','$tgl1','$tgl2')");
        }
        $data=$stmt->fetchAll();
        return $data;
    }

    function getRiById($id_ri){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from scm.f_ri_fby_id('$id_ri')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setRi($id_po,$tgl_ri,$id_supplier,$vat_in,$diskon_ri,$no_inv_supp,$id_branch,$status,$closed){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_ri_ins('$id_po','$tgl_ri','$id_supplier',$vat_in,$diskon_ri,'$no_inv_supp','$id_branch',$status,'$closed')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_ri_ins'];
        }
        return $return;
    }
    function updRi($id_po,$tgl_ri,$id_supplier,$vat_in,$diskon_ri,$no_inv_supp,$id_branch,$status,$closed,$id_ri){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_ri_upd('$id_po','$tgl_ri','$id_supplier',$vat_in,$diskon_ri,'$no_inv_supp','$id_branch',$status,'$closed','$id_ri')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_ri_upd'];
        }
        return $return;
    }
    
    function delRi($id_ri){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_ri_del('$id_ri')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_ri_del'];
        }
        return $return;
    }
    
}