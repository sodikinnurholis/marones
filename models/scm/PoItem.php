<?php
class PoItem extends Zend_Db_Table
{
    protected $_name = 'scm.v_po_item';
    protected $_primary='id_po_item';
    
    function getPoItemtById($id_po_item){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from scm.f_po_item_fby_id('$id_po_item')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function getPoItemtByPo($id_po){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from scm.f_po_item_filter_by_po('$id_po')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setPoItem($id_po,$id_item,$qty_po,$price_po,$id_unit_po,$konv){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_po_item_ins('$id_po','$id_item',$qty_po,$price_po,'$id_unit_po',$konv)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_po_item_ins'];
        }
        return $return;
    }
    function updPoItem($id_po,$id_item,$qty_po,$price_po,$id_unit_po,$konv,$id_po_item){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_po_item_upd('$id_po','$id_item',$qty_po,$price_po,'$id_unit_po',$konv,'$id_po_item')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_po_item_upd'];
        }
        return $return;
    }
    
    function delPoItem($id_po_item){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_po_item_del('$id_po_item')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_po_item_del'];
        }
        return $return;
    }
    
}