<?php
class Supplier extends Zend_Db_Table
{
    protected $_name = 'scm.v_supplier';
    protected $_primary='id_supplier';
    
    function getSupplierById($id_supplier){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from scm.f_supplier_fby_id('$id_supplier')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setSupplier($id_kontak){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_supplier_ins('$id_kontak')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_supplier_ins'];
        }
        return $return;
    }
    function updSupplier($id_kontak,$id_supplier){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_supplier_upd('$id_kontak','$id_supplier')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_supplier_upd'];
        }
        return $return;
    }
    
    function delSupplier($id_supplier){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_supplier_del('$id_supplier')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_supplier_del'];
        }
        return $return;
    }
    
}