<?php
class Invoice1 extends Zend_Db_Table
{
    protected $_name = 'scm.v_invoice1';
    protected $_primary='id_invoice1';
    
    function getInvoice1tById($id_invoice1){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from scm.f_invoice1_fby_id('$id_invoice1')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setInvoice1($id_invoice0,$id_ri,$nominal_invoice){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_invoice1_ins('$id_invoice0','$id_ri',$nominal_invoice)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_invoice1_ins'];
        }
        return $return;
    }
    function updInvoice1($id_invoice0,$id_ri,$nominal_invoice,$id_invoice1){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_invoice1_upd('$id_invoice0','$id_ri',$nominal_invoice,'$id_invoice1')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_invoice1_upd'];
        }
        return $return;
    }
    
    function delInvoice1($id_invoice1){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_invoice1_del('$id_invoice1')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_invoice1_del'];
        }
        return $return;
    }
    
}