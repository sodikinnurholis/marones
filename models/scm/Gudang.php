<?php
class Gudang extends Zend_Db_Table
{
    protected $_name = 'scm.v_gudang';
    protected $_primary='id_gudang';
    
    function getGudangById($id_gudang){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from scm.f_gudang_fby_id('$id_gudang')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setGudang($nm_gudang){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_gudang_ins('$nm_gudang')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_gudang_ins'];
        }
        return $return;
    }
    function updGudang($nm_gudang,$id_gudang){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_gudang_upd('$nm_gudang','$id_gudang')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_gudang_upd'];
        }
        return $return;
    }
    
    function delGudang($id_gudang){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_gudang_del('$id_gudang')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_gudang_del'];
        }
        return $return;
    }
    
}