<?php
class RtnItem extends Zend_Db_Table
{
    protected $_name = 'scm.v_rtn_item';
    protected $_primary='id_rtn_item';
    
    function getRtnItemtById($id_rtn_item){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from scm.f_rtn_item_fby_id('$id_rtn_item')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setRtnItem($id_rtn,$id_ri_item,$qty_rtn){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_rtn_item_ins('$id_rtn','$id_ri_item',$qty_rtn)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_rtn_item_ins'];
        }
        return $return;
    }
    function updRtnItem($id_rtn,$id_ri_item,$qty_rtn,$id_rtn_item){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_rtn_item_upd('$id_rtn','$id_ri_item',$qty_rtn,'$id_rtn_item')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_rtn_item_upd'];
        }
        return $return;
    }
    
    function delRtnItem($id_rtn_item){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_rtn_item_del('$id_rtn_item')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_rtn_item_del'];
        }
        return $return;
    }
    
}