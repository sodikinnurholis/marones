<?php
class Po extends Zend_Db_Table
{
    protected $_name = 'v_po';
    protected $_primary='id_po';
    
    function getPoById($id_po){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from f_po_fby_id('$id_po')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setPo($no_po,$tgl_po,$keterangan,$id_supplier,$vat_in,$diskon_po,$closed,$id_branch){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_po_ins('$no_po','$tgl_po','$keterangan','$id_supplier',$vat_in,$diskon_po,'$closed','$id_branch')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_po_ins'];
        }
        return $return;
    }
    function updPo($no_po,$tgl_po,$keterangan,$id_supplier,$vat_in,$diskon_po,$closed,$id_branch,$id_po){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_po_upd('$no_po','$tgl_po','$keterangan','$id_supplier',$vat_in,$diskon_po,'$closed','$id_branch','$id_po')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_po_upd'];
        }
        return $return;
    }
    
    function delPo($id_po){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from f_po_del('$id_po')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_po_del'];
        }
        return $return;
    }
    
}