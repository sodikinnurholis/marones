<?php
class Invoice0 extends Zend_Db_Table
{
    protected $_name = 'scm.v_invoice0';
    protected $_primary='id_invoice0';
    
    function getInvoice0tById($id_invoice0){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $stmt=$db->query("select * from scm.f_invoice0_fby_id('$id_invoice0')");
        $data=$stmt->fetchAll();
        return $data;
    }
    function setInvoice0($no_invoice_supp,$id_supplier,$tgl_issue,$tgl_jatuh_tempo,$nominal_ri,$diskon,$vat_in,$id_branch,$a_ri,$closed,$a_kredit,$keterangan,$no_faktur_pajak,$status){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_invoice0_ins('$no_invoice_supp','$id_supplier','$tgl_issue','$tgl_jatuh_tempo',$nominal_ri,$diskon,$vat_in,'$id_branch','$a_ri','$closed','$a_kredit','$keterangan','$no_faktur_pajak',$status)");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_invoice0_ins'];
        }
        return $return;
    }
    function updInvoice0($no_invoice_supp,$id_supplier,$tgl_issue,$tgl_jatuh_tempo,$nominal_ri,$diskon,$vat_in,$id_branch,$a_ri,$closed,$a_kredit,$keterangan,$no_faktur_pajak,$status,$id_invoice0){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_invoice0_upd('$no_invoice_supp','$id_supplier','$tgl_issue','$tgl_jatuh_tempo',$nominal_ri,$diskon,$vat_in,'$id_branch','$a_ri','$closed','$a_kredit','$keterangan','$no_faktur_pajak',$status,'$id_invoice0')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_invoice0_upd'];
        }
        return $return;
    }
    
    function delInvoice0($id_invoice0){
        // database
        $db=Zend_Registry::get('dbAdapter');
        $query=$db->query("select * from scm.f_invoice0_del('$id_invoice0')");
        $isset=$query->fetchAll();
        foreach ($isset as $returnData) {
            $return=$returnData['f_invoice0_del'];
        }
        return $return;
    }
    
}